package pl.testy.walidator;

/**
 * Walidacja wprowadzanych danych
 * @author wojtek
 *
 */
public class Walidator {

	/**
	 * walidacja pustych pól
	 * @param param
	 * @return wartość logiczna true dla poprawnych danych
	 */
	public boolean validateEmptyString(String param) {

		if (param != null && !param.equals("") && !param.matches("(.*\\s)+.*")) { // regularne
			return true;
		} else {
			return false;
		}

	}
	/**
	 * walidacja pustych pól
	 * @param param parametr wejściowy
	 * @return wartość logiczna true dla poprawnych danych
	 */
	public boolean validateNull(String param) {

		if (param != null) { // regularne
			return true;
		} else {
			return false;
		}

	}
/**
 * walidacja dlugosci hasla
 * @param param parametr wejściowy
 * @return wartość logiczna true dla poprawnych danych
 */
	public boolean validateLenghtString(String param) {
		if (param.length() > 6) {
			return true;
		} else {
			return false;
		}
	}
/**
 * walidacja formatu adresu email
 * @param param parametr wejściowy
 * @return wartość logiczna true dla poprawnych danych
 */
	public boolean validateEmail(String param) {
		if (param != null
				&& !param.equals("")
				&& param.matches("^[a-zA-Z0-9.\\-_]+@[a-zA-Z0-9\\-.]+\\.[a-zA-Z]{2,4}$")) { // regularne
			return true;
		} else {
			return false;
		}
	}
/**
 * walidacja poprawnie wpisanych haseł
 * @param param1 parametr wejściowy 1
 * @param param2 parametr wejściowy 2
 * @return wartość logiczna true dla poprawnych danych
 */
	public boolean validatePassword(String param1, String param2) {
		if (param1.equals(param2)) { // porównanie hasel
			return true;
		} else {
			return false;
		}
	}

}
