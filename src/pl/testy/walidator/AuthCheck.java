package pl.testy.walidator;

import pl.testy.db.manager.ClassDbManager;
import pl.testy.db.manager.SurveyDbManager;
import pl.testy.db.manager.TestDbManager;
import pl.testy.db.manager.UserDbManager;
/**
 * Klasa Autoryzacji
 * @author wojtek
 *
 */
public class AuthCheck {
/**
 * Autoryzacja dostępu do klasy
 * @param login login użytkownika
 * @param idklasy identyfikatora klasy
 * @return wartośc logiczna true dla autoryzowanych użytkowników
 */
	public boolean checkClassPermisson(String login, int idklasy) {
		
		
		
		UserDbManager mgm = new UserDbManager();
		ClassDbManager cdm = new ClassDbManager();
		
		int iddydaktyk = mgm.getIdByLogin(login);
		boolean checkp = cdm.checkClassPermission(idklasy, iddydaktyk);
		
		return checkp;
		
	}
	/**
	 * Autoryzacja dostępu do wyników
	 * @param idtester identyfikator użytkownika
	 * @param idwyniku identyfikator wyniku
	 * @return wartośc logiczna true dla autoryzowanych użytkowników
	 */
	public boolean checkShowResultPermisson(int idtester, int idwyniku) {

		TestDbManager tdm = new TestDbManager();
		boolean checkp = tdm.checkResultPermission(idtester, idwyniku);
		return checkp;
	}
/**
 * Autoryzacja dostępu do testu
 * @param login login użytkownika
 * @param idtestu identyfikator testu
 * @return wartośc logiczna true dla autoryzowanych użytkowników
 */
	public boolean checkTestPermisson(String login, int idtestu) {

		TestDbManager tdm = new TestDbManager();
		boolean checkp = tdm.checkTestPermission(login, idtestu);
		return checkp;
	}
	/**
	 * Autoryzacja dostępu do ankiety
	 * @param login login użytkownika
	 * @param idankiety identyfikator ankiety
	 * @return wartośc logiczna true dla autoryzowanych użytkowników
	 */
	public boolean checkSurveyPermisson(String login, int idankiety) {

		SurveyDbManager sdm = new SurveyDbManager();
		boolean checkp = sdm.checkSurveyPermission(login, idankiety);
		return checkp;
	}
	/**
	 * Autoryzacja dostępu do testera
	 * @param login login użytkownika
	 * @param iduser identyfikator użytkownika
	 * @return wartośc logiczna true dla autoryzowanych użytkowników
	 */
public boolean checkUsersPermisson(String login, int iduser){
	
	UserDbManager mgm = new UserDbManager();

	
	int iddydaktyk = mgm.getIdByLogin(login);
	boolean checkp = mgm.checkUserBelongsToAdmin(iddydaktyk, iduser);
	
	if(iddydaktyk == iduser){
		
		checkp = true; 
	}
	
	return checkp;
	
}
}


