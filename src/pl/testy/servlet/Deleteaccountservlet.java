package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.UserDbManager;
import pl.testy.model.UserFull;
import pl.testy.walidator.AuthCheck;

/**
 * Servlet implementation class Deleteaccountservlet
 */
@WebServlet("/deleteaccountservlet")
public class Deleteaccountservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Deleteaccountservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		String login = request.getUserPrincipal().getName();
		String message = "";

		AuthCheck acheck = new AuthCheck();
		if (!acheck.checkUsersPermisson(login, id)) {

			message = "Brak dostępu !!!";
			String url = "/index.jsp";
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
			return;
		}

		UserDbManager mgm = new UserDbManager(); // tworzy obiekt klasy
		// UserDbManager
		UserFull uf = mgm.getProfileById(Integer.parseInt(request
				.getParameter("id"))); // wywoluje metode wszystko co przychodzi
		// w request jest w formie string->
		// parse

		request.setAttribute("useredit", uf);
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/admin/deleteAccount.jsp");
		dispatcher.forward(request, response);
		// TODO Auto-generated method stub
	}
		// TODO Auto-generated method stub
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String message = "";
		String url = "/login/logout.jsp";
	try {
		
		UserDbManager mgm = new UserDbManager();
		int iduser = Integer.parseInt(request.getParameter("iduser"));
		mgm.deleteUser(iduser);
		
		message = "Użytkownik został usunięty";
	} catch (Exception e) {
		message = "Wyjątek krytyczny";// TODO: handle exception
	} finally {
		request.setAttribute("url", url);
		request.setAttribute("message", message);
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/files/statusoperacji.jsp");
		dispatcher.forward(request, response);
	}

	// TODO Auto-generated method stub
}
		
		
		// TODO Auto-generated method stub
	}


