package pl.testy.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.TestDbManager;
import pl.testy.model.Odpowiedz;
import pl.testy.model.Pytanie;

/**
 * Servlet implementation class maketestservlet
 */
@WebServlet("/admin/maketestservlet")
public class Maketestservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	// metoda do ustawiania atrybutow
	private void setAllAttribute(HttpServletRequest request) {

		Integer iloscpytan = Integer.parseInt(request.getParameter("ilosc"));

		request.setAttribute("iloscpytan", iloscpytan);

		for (int i = 0; i < iloscpytan; i++) {
			Integer iloscodpowiedzi = Integer.parseInt(request
					.getParameter("iloscodpowiedzi" + i));
			request.setAttribute("iloscodpowiedzi" + i, iloscodpowiedzi);
		}
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		setAllAttribute(request);
		String message = "";
		String url = "/admin/index.jsp";
		String submit = request.getParameter("submit");
		Integer iloscpytan = Integer.parseInt(request.getParameter("ilosc"));

		try {

			if ("dodaj pytanie".equals(submit)) {

				request.setAttribute("iloscpytan", iloscpytan + 1);

				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/admin/makeTest.jsp");
				dispatcher.forward(request, response);

			}

			for (int i = 0; i < iloscpytan; i++) {

				String submit_add_anserw = request
						.getParameter("submit_add_anserw" + i);
				String submit_delete_anserw = request
						.getParameter("submit_delete_anserw" + i);

				if (submit_add_anserw != null) {
					Integer iloscodpowiedzi = Integer.parseInt(request
							.getParameter("iloscodpowiedzi" + i));
					request.setAttribute("iloscodpowiedzi" + i,
							iloscodpowiedzi + 1);

					RequestDispatcher dispatcher = getServletContext()
							.getRequestDispatcher("/admin/makeTest.jsp");
					dispatcher.forward(request, response);

				}
				if (submit_delete_anserw != null) {

					Integer iloscodpowiedzi = Integer.parseInt(request
							.getParameter("iloscodpowiedzi" + i));
					request.setAttribute("iloscodpowiedzi" + i,
							iloscodpowiedzi - 1);

					RequestDispatcher dispatcher = getServletContext()
							.getRequestDispatcher("/admin/makeTest.jsp");
					dispatcher.forward(request, response);

				}
			}

			if ("usun pytanie".equals(submit)) {
				System.out.println("przed");
				System.out.println(iloscpytan);
				request.setAttribute("iloscpytan", iloscpytan - 1);
				request.setAttribute("ilosc", iloscpytan - 1);
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/admin/makeTest.jsp");
				dispatcher.forward(request, response);

			}

			if ("zapisz zmiany".equals(submit)) {

				String nazwa = request.getParameter("f_name");
				String maxpunkty = request.getParameter("max_points");
				String czastrwania = request.getParameter("timer");

				int maxpunktyint = Integer.parseInt(maxpunkty);
				int czastrwaniaint = Integer.parseInt(czastrwania);
				String opis = request.getParameter("f_desc");

				// boolean pustanazwa = walidator.validateEmptyString(nazwa);
				// boolean pustepytanie =
				// walidator.validateEmptyString(pierwszepytanie);
				// boolean pustaodpowiedz =
				// walidator.validateNull(pierwszaodpowiedz);

				// System.out.println(pustepytanie);
				// System.out.println(pustaodpowiedz);

				String logindydaktyka = request.getUserPrincipal().getName();
				List<Pytanie> listapytan = new ArrayList<Pytanie>();

				for (int i = 0; i < iloscpytan; i++) {
					Pytanie p = new Pytanie();
					List<Odpowiedz> listaodpowiedzi = new ArrayList<Odpowiedz>();
					p.setTresc(request.getParameter("f_question[" + i + "]"));
					p.setFoto(request.getParameter("foto[" + i + "]"));
					p.setVideo(request.getParameter("video[" + i + "]"));

					Integer iloscodpowiedzi = Integer.parseInt(request
							.getParameter("iloscodpowiedzi" + i));
					for (int j = 0; j < iloscodpowiedzi; j++) {
						Odpowiedz o = new Odpowiedz();
						o.setPunktacja(request.getParameter("f_points[" + i
								+ "][" + j + "]"));
						o.setTresc(request.getParameter("f_answer[" + i + "]["
								+ j + "]"));

						listaodpowiedzi.add(o);
					}

					p.setListaodpowiedz(listaodpowiedzi);
					listapytan.add(p);

				}

				TestDbManager TestDbManager = new TestDbManager();
				TestDbManager.saveTest(nazwa, opis, logindydaktyka, listapytan,
						maxpunktyint, czastrwaniaint);
				message = "Test zapisany";

				request.setAttribute("url", url);
				request.setAttribute("message", message);
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/files/statusoperacji.jsp");
				dispatcher.forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace();
			message += e;// TODO: handle exception
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
		} 
	}

}
