package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.ClassDbManager;
import pl.testy.model.Grupa;

/**
 * Servlet implementation class Editgroupservlet
 */
@WebServlet("/admin/editgroupservlet")
public class Editgroupservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Editgroupservlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		ClassDbManager cdm = new ClassDbManager();

		Grupa g = cdm
				.getGroupById(Integer.parseInt(request.getParameter("id")));

		request.setAttribute("groupedit", g);
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/admin/editGroup.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int idklasy = Integer.parseInt(request.getParameter("idklasy"));
		int idgrupy = Integer.parseInt(request.getParameter("id"));
		String message = "";
		String nazwagrupy = request.getParameter("nazwagrupy");
		String submit = request.getParameter("submit");
		String nazwaklasy = request.getParameter("nazwaklasy");		
		ClassDbManager cdm = new ClassDbManager();
		boolean walidacjanazwy = cdm.checkGroup(nazwagrupy, idklasy);
		String url = "/admin/editclassservlet?id="+idklasy+"";
		try {

			if ("zapisz zmiany".equals(submit)) {

				if (!walidacjanazwy) {

					cdm.updateGroupName(nazwagrupy, idgrupy);
					message = "Dane zapisane ";

				} else {
					message = "Błąd : Już posiadasz grupę o tej nazwie w klasie o nazwie <b>"+nazwaklasy+"</b>! ";

				}

			}
			if ("usun".equals(submit)) {
				cdm.deleteGroup(idgrupy);
				message = "Grupa o nazwie <b>"+nazwagrupy+"</b> została usunięta";
			}
			if ("zapisz grupe".equals(submit)) {

				if (!walidacjanazwy) {

					cdm.saveGroup(nazwagrupy, idklasy);
					message = "Dane zapisane, dodano grupę o nazwie <b>"+nazwagrupy+"</b> do klasy <b>"+nazwaklasy+"</b>";

				} else {
					message = "Błąd : Już posiadasz grupę <b>"+nazwagrupy+"</b> w klasie <b>"+nazwaklasy+"</b>! ";

				}

			}
		} catch (Exception e) {
			message = "Wyjątek Krytyczny"; // TODO: handle exception
			
		} finally {
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response); // TODO: handle exception

		}

	}

}
