package pl.testy.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.TestDbManager;
import pl.testy.db.manager.UserDbManager;
import pl.testy.model.Pytanie;
import pl.testy.walidator.AuthCheck;

/**
 * Servlet implementation class Asassmenttestservlet
 */
@WebServlet("/user/assasmenttestservlet")
public class Assasmenttestservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Assasmenttestservlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int id = Integer.parseInt(request.getParameter("id"));
		String login = request.getUserPrincipal().getName();
		String message = "";
		
		AuthCheck acheck = new AuthCheck();
		if (!acheck.checkTestPermisson(login, id)) {

			message = "Brak dostępu !!!";
			String url = "/index.jsp";
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		
		
		UserDbManager mgm = new UserDbManager();

		int idtestera = mgm.getIdByLogin(login);

		TestDbManager tdm = new TestDbManager();
		tdm.starttest(id, idtestera);
		
		int time = tdm.getTestDuration(id);
		
		List<Integer> listaid = tdm.getIdPytan(id);
		
		//Mechanizm losowania
		 Random r = new Random();
		  
		 int l = listaid.size();  //losuje liczbę z listy
		 
		  for(int i = 0 ; i < l; i++){                  //pętla dla ilosci elementow w liscie
		   int liczba = r.nextInt(listaid.size());
		
		   Integer s = listaid.remove(liczba);        //bierzemy element usuwamy
		   listaid.add(s);                              //dodajemy go na koniec 
		   
		  }
		
		Pytanie p = tdm.getPytanie(listaid.get(0));

		request.setAttribute("czas", time+":00");
		request.setAttribute("listapytan", listaid);
		request.setAttribute("pytanie", p);
		request.setAttribute("indexpytanie", 0);
		request.setAttribute("udzieloneodpowiedzi", new ArrayList<Integer>());
		request.setAttribute("testId", id);

		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/user/startTest.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		int testId = Integer.parseInt(request.getParameter("testId"));
		int indexpytanie = Integer.parseInt(request
				.getParameter("indexpytanie"));
		int iloscodpowiedzi = Integer.parseInt(request
				.getParameter("iloscodpowiedzi"));
		int iloscpytan = Integer.parseInt(request.getParameter("iloscpytan"));
		String czas = request.getParameter("czas");

		List<Integer> listaPytan = new ArrayList<Integer>();
		for (int i = 0; i < iloscpytan; i++) {
			int pytanieId = Integer.parseInt(request.getParameter("idpytania_"
					+ i));
			listaPytan.add(pytanieId);
		}

		List<Integer> listaOdp = new ArrayList<Integer>();
		for (int i = 0; i < iloscodpowiedzi; i++) {
			int odpId = Integer.parseInt(request.getParameter("idodpowiedzi_"
					+ i));
			listaOdp.add(odpId);
		}

		int odpNaPytanie = Integer.parseInt(request.getParameter("odpowiedz"));
		listaOdp.add(odpNaPytanie);
		indexpytanie++;

		TestDbManager tdm = new TestDbManager();
		if (indexpytanie == listaPytan.size()) {

			UserDbManager udbm = new UserDbManager();
			int idTester = udbm.getIdByLogin(request.getUserPrincipal().getName());
			
			
			
			long start = tdm.getStartTest(testId, idTester).getTime();
			long stop = System.currentTimeMillis();
			long duration = stop-start;
			int testDuration = tdm.getTestDuration(testId) * 60 * 1000;
			
			int wynik = 0;
			if(testDuration > duration){
				wynik = tdm.getWynikOdpowiedzi(listaOdp);	
			}

			tdm.updateWynik(request.getUserPrincipal().getName(), testId, wynik);

			request.setAttribute("wynik", wynik);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/user/wynikTest.jsp");
			dispatcher.forward(request, response);

			return;
		}

		Pytanie p = tdm.getPytanie(listaPytan.get(indexpytanie));

		request.setAttribute("czas", czas);
		request.setAttribute("testId", testId);
		request.setAttribute("listapytan", listaPytan);
		request.setAttribute("pytanie", p);
		request.setAttribute("indexpytanie", indexpytanie);
		request.setAttribute("udzieloneodpowiedzi", listaOdp);

		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/user/startTest.jsp");
		dispatcher.forward(request, response);
	}

}
