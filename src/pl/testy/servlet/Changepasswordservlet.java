package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.converter.MD5Genegate;
import pl.testy.db.manager.UserDbManager;

/**
 * Servlet implementation class Changepasswordservlet
 */
@WebServlet("/changepasswordservlet")
public class Changepasswordservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Changepasswordservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int iduser = Integer.parseInt(request.getParameter("iduser"));
		String password = request.getParameter("oldpassword");
		String message = "";
		String url = "/user/changePassword.jsp";
		
		
		
		
		UserDbManager udm = new UserDbManager();
		String dbpassword = udm.getPassword(iduser);
		
		String role = udm.getRoleByIdUsers(iduser);
		

		if(role.equals("admin")){
			
			url = "/admin/changePassword.jsp";
		}

		MD5Genegate md5Genegate = new MD5Genegate();
		password = md5Genegate.generate(password);
		
		try {
			
			if (password.equals(dbpassword)){
				
				String newpassword = request.getParameter("newpassword");
				String retypenewpassword = request.getParameter("retypenewpassword");
				
				if (newpassword.equals(retypenewpassword)){
					
					newpassword = md5Genegate.generate(newpassword);
					udm.updatePassword(iduser, newpassword);
					
					message = "Hasło zostało zmienione";
				} else {
					message = "Niepoprawne powtórzone hasło";
				}
				
				
			} else {
				message = "Niepoprawne stare hasło";
			} 
			
			
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
		}
		

		
		// TODO Auto-generated method stub
	}

}
