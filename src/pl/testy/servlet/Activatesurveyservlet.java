package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.SurveyDbManager;
import pl.testy.model.SurveyHeader;

/**
 * Servlet implementation class activatesurveyservlet
 */
@WebServlet("/admin/activatesurveyservlet")
public class Activatesurveyservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Activatesurveyservlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		
		int idtestu = Integer.parseInt(request.getParameter("id"));
		
		SurveyDbManager sdm = new SurveyDbManager();
		SurveyHeader th = sdm.showSurvey(idtestu); // wywoluje metode wszystko co przychodzi
		// w request jest w formie string->
		// parse

		request.setAttribute("testedit", th);
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/admin/editSurvey.jsp");
		dispatcher.forward(request, response);

		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String message = "";
		String url = "/admin/showSurveys.jsp";
		String nazwa = request.getParameter("nazwa");
		String stan = request.getParameter("stan");
	
		
		//Sprawdzamy uprawnienia

		

		int idankiety = Integer.parseInt(request.getParameter("idtestu"));
		
		try {
			
			//Update danych testu
			SurveyDbManager sdm = new SurveyDbManager();
			sdm.updateSurvey(idankiety, nazwa, stan);
			
			
			message = "Dane zapisane..";
			request.setAttribute("message", message);
			request.setAttribute("url", url);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			
			message = "Błąd";
			request.setAttribute("message", message);
			request.setAttribute("url", url);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);// TODO: handle exception
		}
		
		// TODO Auto-generated method stub
	}

}
