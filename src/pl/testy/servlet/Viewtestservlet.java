package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.TestDbManager;
import pl.testy.model.TestHeader;
import pl.testy.walidator.AuthCheck;

/**
 * Servlet implementation class Viewtestservlet
 */
@WebServlet("/user/viewtestservlet")
public class Viewtestservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Viewtestservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int idtestu = Integer.parseInt(request.getParameter("id"));
		
		String message = "";
		String login = request.getUserPrincipal().getName();
		
		AuthCheck acheck = new AuthCheck();
		if (!acheck.checkTestPermisson(login, idtestu)) {

			message = "Brak dostępu !!!";
			String url = "/index.jsp";
			request.setAttribute("url", url);
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		TestDbManager tdm = new TestDbManager();
		TestHeader th = tdm.getTestById(idtestu);
		
		request.setAttribute("testheader",th);

		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/user/doTest.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
