package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.UserDbManager;

/**
 * Servlet implementation class maketestservlet
 */
@WebServlet("/acceptservlet")
public class Acceptservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String token = request.getParameter("token");

		UserDbManager userDbManager = new UserDbManager();
		boolean result = userDbManager.acceptUserToken(token);
		String url = "/index.jsp";
		String message = "";
		if (result) {
			message = "Aktywacja poprawna, proszę się zalogować";
		} else {
			message = "Niepoprawny token";
		}
		request.setAttribute("url", url);
		request.setAttribute("message", message);
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/files/statusoperacji.jsp");
		dispatcher.forward(request, response);
	}

}
