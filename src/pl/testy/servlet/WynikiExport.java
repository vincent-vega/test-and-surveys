package pl.testy.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import pl.testy.db.manager.TestDbManager;
import pl.testy.model.Wynik;

/**
 * Servlet implementation class Test
 */
@WebServlet("/WynikiExport")
public class WynikiExport extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		JasperReport jasperReport;
		JasperPrint jasperPrint;
		
		TestDbManager manager = new TestDbManager(); 
		int idtestu = Integer.parseInt(request.getParameter("idTestu"));
		List<Wynik> wynikiList = manager.wynikList(idtestu);
		
		try {

			Map<String, Object> param = new HashMap<String, Object>();
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document document = db.newDocument();

			Element book = document.createElement("wyniki");
			document.appendChild(book);
			
			for (Wynik w: wynikiList) {
				Element osoba = document.createElement("osoba");
				Element imie = document.createElement("imie");
				imie.appendChild(document.createTextNode(w.getImie()));
				osoba.appendChild(imie);

				Element nazwisko = document.createElement("nazwisko");
				nazwisko.appendChild(document.createTextNode(w.getNazwisko()));

				Element telefon = document.createElement("wynik");
				telefon.appendChild(document.createTextNode(w.getZdobytywynik()+""));

				osoba.appendChild(nazwisko);
				osoba.appendChild(telefon);
				book.appendChild(osoba);
			}

			param.put(JRXPathQueryExecuterFactory.PARAMETER_XML_DATA_DOCUMENT,
					document);

			jasperReport = JasperCompileManager
					.compileReport("C:\\Users\\wojtek\\Desktop\\workspace2\\raporty\\raportWynikow.jrxml");
			jasperPrint = JasperFillManager.fillReport(jasperReport, param);
			
			JRExporter exporter = null;

			ServletOutputStream os = response.getOutputStream();

			exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
			exporter.exportReport();
			
			response.setContentType("application/octet-stream");

			// response.setContentLength(10000000);

			response.setHeader("Content-Disposition",
					"attachment; filename=\"wyniki.pdf\"");

		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

}
