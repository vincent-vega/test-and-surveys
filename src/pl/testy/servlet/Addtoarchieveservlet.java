package pl.testy.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pl.testy.db.manager.TestDbManager;
import pl.testy.db.manager.UserDbManager;
import pl.testy.walidator.AuthCheck;

/**
 * Servlet implementation class Addtoarchieve
 */
@WebServlet("/addtoarchieveservlet")
public class Addtoarchieveservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Addtoarchieveservlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		TestDbManager tdm = new TestDbManager(); 
		UserDbManager udm = new UserDbManager();
		int iduser = udm.getIdByLogin(request.getUserPrincipal().getName());
		int idwynik = Integer.parseInt(request.getParameter("id"));
		String message = "Dazne zapisane";
		
		AuthCheck acheck = new AuthCheck();
		if (!acheck.checkShowResultPermisson(iduser, idwynik)) {

			message = "Brak dostępu !!!";
			request.setAttribute("message", message);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/files/statusoperacji.jsp");
			dispatcher.forward(request, response);
			return;
		}
		

		tdm.addResultToArchieve(iduser, idwynik);
		
		String url = "/user/showResult.jsp";
		request.setAttribute("url", url);	
		request.setAttribute("message", message);
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/files/statusoperacji.jsp");
		dispatcher.forward(request, response);
		
		
		
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
