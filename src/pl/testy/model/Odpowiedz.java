package pl.testy.model;

public class Odpowiedz {
	
	private String tresc;
	private String punktacja;
	private int idodpowiedzi;
	private int ilosc_klikniec;
	
	public String getTresc() {
		return tresc;
	}
	public void setTresc(String tresc) {
		this.tresc = tresc;
	}
	public String getPunktacja() {
		return punktacja;
	}
	public void setPunktacja(String punktacja) {
		this.punktacja = punktacja;
	}
	public int getIdodpowiedzi() {
		return idodpowiedzi;
	}
	public void setIdodpowiedzi(int idodpowiedzi) {
		this.idodpowiedzi = idodpowiedzi;
	}
	public int getIlosc_klikniec() {
		return ilosc_klikniec;
	}
	public void setIlosc_klikniec(int ilosc_klikniec) {
		this.ilosc_klikniec = ilosc_klikniec;
	}

}
