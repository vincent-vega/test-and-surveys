package pl.testy.model;


public class Grupa {

	private String nazwagrupy;
	private int idklasygrupy;
	private int idgrupysql;
	public String getNazwagrupy() {
		return nazwagrupy;
	}
	public void setNazwagrupy(String nazwagrupy) {
		this.nazwagrupy = nazwagrupy;
	}
	public int getIdKlasygrupy() {
		return idklasygrupy;
	}
	public void setIdKlasygrupy(int idgrupy) {
		this.idklasygrupy = idgrupy;
	}
	public int getIdgrupysql() {
		return idgrupysql;
	}
	public void setIdgrupysql(int idgrupysql) {
		this.idgrupysql = idgrupysql;
	}

}
