package pl.testy.model;

import java.util.List;

public class Ankieta {
private int idankiety;
private String nazwa;
private String stan;
private List<Pytanie> listapytan;
public int getIdankiety() {
	return idankiety;
}
public void setIdankiety(int idankiety) {
	this.idankiety = idankiety;
}
public String getNazwa() {
	return nazwa;
}
public void setNazwa(String nazwa) {
	this.nazwa = nazwa;
}
public String getStan() {
	return stan;
}
public void setStan(String stan) {
	this.stan = stan;
}
public List<Pytanie> getListapytan() {
	return listapytan;
}
public void setListapytan(List<Pytanie> listapytan) {
	this.listapytan = listapytan;
}


}
