package pl.testy.db.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pl.testy.model.Ankieta;
import pl.testy.model.Odpowiedz;
import pl.testy.model.Pytanie;
import pl.testy.model.SurveyHeader;
/**
 * Klasa obsługująca wymiane informacji z bazą danych dla ankiet i informacji z nią związanych
 * @author wojtek
 *
 */
public class SurveyDbManager extends Dbmanager {
/**
 * Zapisywanie ankiety
 * @param tytul nazwa ankiety
 * @param login login użytkownika
 * @param listapytan lista pytan
 */
	public void saveSurvey(String tytul, String login, List<Pytanie> listapytan) {

		UserDbManager userDbManager = new UserDbManager();
		GenerateIdSurvey generator = GenerateIdSurvey.getInstance(); // odniesienie
																		// do
																		// stworzonego
																		// obiektu

		try {

			int iddydaktyk = userDbManager.getIdByLogin(login);
			int idankiety = generator.generateSurveyID();

			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("INSERT INTO ankiety (id_ankiety, id_autor, nazwa_ankiety, stan) VALUES (?,?,?,'N');");

			statement.setInt(1, idankiety);
			statement.setInt(2, iddydaktyk);
			statement.setString(3, tytul);
			statement.execute();

			for (Pytanie pytanie : listapytan) {

				int idpytanieankiety = generator.generateSurveyQuestionID();

				PreparedStatement statement2 = connect
						.prepareStatement("INSERT INTO pytania_ankiety(id_pytania_ankiety, id_ankiety,nazwa_pytania,foto,video) VALUES (?,?,?,?,?);");
				statement2.setInt(1, idpytanieankiety);
				statement2.setInt(2, idankiety);
				statement2.setString(3, pytanie.getTresc());
				statement2.setString(4, pytanie.getFoto());
				statement2.setString(5, pytanie.getVideo());
				statement2.execute();

				for (Odpowiedz odpowiedz : pytanie.getListaodpowiedz()) {

					PreparedStatement statement3 = connect
							.prepareStatement("INSERT INTO odpowiedz_ankiety(id_pytania_ankiety,tresc_odpowiedzi,ilosc_klikniec) VALUES (?,?,?);");
					statement3.setInt(1, idpytanieankiety);
					statement3.setString(2, odpowiedz.getTresc());
					statement3.setInt(3, 0);
					statement3.execute();
				}

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
/**
 * Przygotowywanie informacji o ankiecie do wyświetlenia
 * @param idankiety identyfikator ankiety
 * @return obiekt SurveyHeader
 */
	public SurveyHeader showSurvey(int idankiety) {

		SurveyHeader sh = new SurveyHeader();

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT id_ankiety, nazwa_ankiety, stan FROM ankiety WHERE id_ankiety=? ;");
			statement.setInt(1, idankiety);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			if (wynik.next()) { // przeszukiwanie wierszy

				sh.setIdankiety(wynik.getInt("id_ankiety"));
				sh.setNazwa(wynik.getString("nazwa_ankiety"));
				sh.setStan(wynik.getString("stan"));

			}

			statement.close();
			wynik.close();
			connect.close();
			return sh;

		} catch (SQLException e) {
			e.printStackTrace();
			return sh;
		}
	}
	/**
	 * Walidacja uprawnień do danej ankiety
	 * @param login login uzytkownika
	 * @param idankiety identyfikator ankiety
	 * @return wartość logiczna true dla istniejącej ankiety danego uzytkownika
	 */
	public boolean checkSurveyPermission(String login, int idankiety) {

		boolean nazwajest = false;
		UserDbManager udm = new UserDbManager();
		
		int iddydaktyka = udm.getIdDydaktykByTesterLogin(login);
		
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT nazwa_ankiety FROM ankiety WHERE id_ankiety=? AND id_autor=?;");
			statement.setInt(1, idankiety);
			statement.setInt(2, iddydaktyka);

			ResultSet wynik = statement.executeQuery();
			if (wynik.next()) {
				nazwajest = true;
			}
			statement.close();
			connect.close();
			return nazwajest;
		} catch (SQLException e) {
			e.printStackTrace();
			return nazwajest;
		}

	}
	/**
	 * Zebranie informacji o wynikach ankiety
	 * @param idankiety identyfikator ankiety
	 * @return obiekt Ankieta
	 */
	public Ankieta surveyResult(int idankiety) {

		Ankieta s = new Ankieta();
		s.setListapytan(new ArrayList<Pytanie>());

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT id_ankiety, nazwa_ankiety, stan FROM ankiety WHERE id_ankiety=? ;");
			statement.setInt(1, idankiety);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			if (wynik.next()) { // przeszukiwanie wierszy

				s.setIdankiety(wynik.getInt("id_ankiety"));
				s.setNazwa(wynik.getString("nazwa_ankiety"));
				s.setStan(wynik.getString("stan"));

				PreparedStatement statement2 = connect
						.prepareStatement("SELECT id_pytania_ankiety FROM pytania_ankiety WHERE id_ankiety=?");
				statement2.setInt(1, idankiety);
				ResultSet wynik2 = statement2.executeQuery();
				while (wynik2.next()) {

					Pytanie p = getPytanie(wynik2.getInt("id_pytania_ankiety"));
					s.getListapytan().add(p);
				}

			}

			statement.close();
			wynik.close();
			connect.close();
			return s;

		} catch (SQLException e) {
			e.printStackTrace();
			return s;
		}
	}
/**
 * Pobranie identyfikatorów pytań ankiety
 * @param idankiety identyfikator ankiety
 * @return Lista identyfikatorów pytań ankiety
 */
	public List<Integer> getIdPytan(int idankiety) {
		List<Integer> listaidpytan = new ArrayList<Integer>();
		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT id_pytania_ankiety FROM pytania_ankiety  WHERE id_ankiety = ?;");
			statement.setInt(1, idankiety);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy
				listaidpytan.add(wynik.getInt("id_pytania_ankiety"));

			}

			statement.close();
			wynik.close();
			connect.close();
			return listaidpytan;

		} catch (SQLException e) {
			e.printStackTrace();
			return listaidpytan;
		}
	}
/**
 * Pobranie informacji o pytaniu oraz jego odpowiedziach
 * @param idpytania identyfikator pytania
 * @return obiekt Pytanie
 */
	public Pytanie getPytanie(int idpytania) {
		Pytanie p = new Pytanie();
		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT p.nazwa_pytania,p.foto,p.video,o.tresc_odpowiedzi,o.ilosc_klikniec,o.id_odpowiedz_ankiety FROM pytania_ankiety p, odpowiedz_ankiety o  WHERE p.id_pytania_ankiety = o.id_pytania_ankiety AND p.id_pytania_ankiety=?;");
			statement.setInt(1, idpytania);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
			// dwuwymiarowa tablice
			List<Odpowiedz> listao = new ArrayList<Odpowiedz>();

			if (wynik.next()) { // przeszukiwanie wierszy

				p.setTresc(wynik.getString("p.nazwa_pytania"));
				p.setFoto(wynik.getString("p.foto"));
				p.setVideo(wynik.getString("p.video"));
				Odpowiedz o = new Odpowiedz();
				o.setTresc(wynik.getString("tresc_odpowiedzi"));
				o.setIlosc_klikniec(wynik.getInt("o.ilosc_klikniec"));
				o.setIdodpowiedzi(wynik.getInt("o.id_odpowiedz_ankiety"));
				listao.add(o);
			}

			while (wynik.next()) { // przeszukiwanie wierszy

				Odpowiedz o = new Odpowiedz();
				o.setTresc(wynik.getString("tresc_odpowiedzi"));
				o.setIlosc_klikniec(wynik.getInt("o.ilosc_klikniec"));
				o.setIdodpowiedzi(wynik.getInt("o.id_odpowiedz_ankiety"));
				listao.add(o);
			}
			p.setListaodpowiedz(listao);
			statement.close();
			wynik.close();
			connect.close();
			return p;

		} catch (SQLException e) {
			e.printStackTrace();
			return p;
		}
	}
/**
 * Uaktualnienie danych ankiety
 * @param idankiety identyfikator ankiety
 * @param nazwa nowa nazwa ankiety
 * @param stan status
 */
	public void updateSurvey(int idankiety, String nazwa, String stan) {
		try {

			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("UPDATE ankiety SET nazwa_ankiety=?, stan=? WHERE id_ankiety=? ;");
			statement.setString(1, nazwa);
			statement.setString(2, stan);
			statement.setInt(3, idankiety);
			statement.execute();
			statement.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();// TODO: handle exception
		}
	}
/**
 * Uaktualnienie wyniku danej ankiety
 * @param listaodpowiedzi lista identyfikatorów odpowiedzi
 */
	public void surveyFinish(List<Integer> listaodpowiedzi) {

		try {
			Connection connect = startConnections();

			for (Integer integer : listaodpowiedzi) {

				PreparedStatement statement = connect
						.prepareStatement("update odpowiedz_ankiety set ilosc_klikniec = ilosc_klikniec+1  WHERE id_odpowiedz_ankiety = ? ;");
				statement.setInt(1, integer);
				statement.executeUpdate(); // result set zwraca
				statement.close();
			}

			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
/**
 * Pobranie informacji o ankietach dla użytkownika dydaktyk
 * @param logindydaktyka login użytkownika
 * @return lista obiektów SurveyHeader
 */
	public List<SurveyHeader> surveyList(String logindydaktyka) {

		List<SurveyHeader> surveylist = new ArrayList<SurveyHeader>();

		try {
			Connection connect = startConnections();

			UserDbManager mgm = new UserDbManager();

			int iddydaktyka = mgm.getIdByLogin(logindydaktyka);

			PreparedStatement statement = connect
					.prepareStatement("SELECT id_ankiety, nazwa_ankiety, stan FROM ankiety WHERE id_autor=? AND NOT stan='AR' ;");
			statement.setInt(1, iddydaktyka);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				SurveyHeader sh = new SurveyHeader();

				sh.setIdankiety(wynik.getInt("id_ankiety"));
				sh.setNazwa(wynik.getString("nazwa_ankiety"));
				sh.setStan(wynik.getString("stan"));
				surveylist.add(sh); // dorzucanie do listy
			}
			statement.close();
			wynik.close();
			connect.close();
			return surveylist;

		} catch (SQLException e) {
			e.printStackTrace();
			return surveylist;
		}
	}
/**
 * Pobranie informacji o ankietach dla użytkownika tester
 * @param logintestera login użytkownika
 * @return lista obiektów SurveyHeader
 */
	public List<SurveyHeader> surveyListUser(String logintestera) {

		List<SurveyHeader> surveylist = new ArrayList<SurveyHeader>();

		try {
			Connection connect = startConnections();

			UserDbManager mgm = new UserDbManager();

			int dydaktykid = mgm.getIdDydaktykByTesterLogin(logintestera);

			PreparedStatement statement = connect
					.prepareStatement("SELECT id_ankiety, nazwa_ankiety, stan FROM ankiety WHERE id_autor=? AND stan='A';");
			statement.setInt(1, dydaktykid);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				SurveyHeader sh = new SurveyHeader();

				sh.setIdankiety(wynik.getInt("id_ankiety"));
				sh.setNazwa(wynik.getString("nazwa_ankiety"));
				sh.setStan(wynik.getString("stan"));
				surveylist.add(sh); // dorzucanie do listy
			}
			statement.close();
			wynik.close();
			connect.close();
			return surveylist;

		} catch (SQLException e) {
			e.printStackTrace();
			return surveylist;
		}
	}
/**
 * Pobranie informacji o ankietach przeniesionych do archiwum
 * @param logindydaktyka login użytkownika dydaktyk
 * @return lista obiektów SurveyHeader
 */
	public List<SurveyHeader> surveyListArchieve(String logindydaktyka) {

		List<SurveyHeader> surveylist = new ArrayList<SurveyHeader>();

		try {
			Connection connect = startConnections();

			UserDbManager mgm = new UserDbManager();

			int iddydaktyka = mgm.getIdByLogin(logindydaktyka);

			PreparedStatement statement = connect
					.prepareStatement("SELECT id_ankiety, nazwa_ankiety, stan FROM ankiety WHERE id_autor=? AND stan='AR' ");
			statement.setInt(1, iddydaktyka);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				SurveyHeader sh = new SurveyHeader();

				sh.setIdankiety(wynik.getInt("id_ankiety"));
				sh.setNazwa(wynik.getString("nazwa_ankiety"));
				sh.setStan(wynik.getString("stan"));
				surveylist.add(sh); // dorzucanie do listy
			}
			statement.close();
			wynik.close();
			connect.close();
			return surveylist;

		} catch (SQLException e) {
			e.printStackTrace();
			return surveylist;
		}
	}

}
