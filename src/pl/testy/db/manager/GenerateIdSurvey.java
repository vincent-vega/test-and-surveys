package pl.testy.db.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
/**
 * Klasa generująca identyfikatory dla ankiety i pytań zawiera prywatny konstruktor oparta na wzorcu singleton
 * @author wojtek
 *
 */
public class GenerateIdSurvey extends Dbmanager {

	// prywatny konstruktor to jest wykorzystywane w klasie w ktorej jest
	private GenerateIdSurvey() {

		try {

			Connection connect = startConnections();
			PreparedStatement statement = connect.prepareStatement("SELECT max(id_ankiety) FROM ankiety;");
			ResultSet wynik = statement.executeQuery();
			
			if (wynik.next()){
				ankietaid = wynik.getInt(1);
			} 
			statement = connect.prepareStatement("SELECT max(id_pytania_ankiety) FROM pytania_ankiety;");
			 wynik = statement.executeQuery();
			
			if (wynik.next()){
				pytanieankietyid = wynik.getInt(1);
			}
			statement.close();
			wynik.close();
			connect.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private int ankietaid;
	private int pytanieankietyid;

	// prywatne statyczne pole
	private static GenerateIdSurvey generator = new GenerateIdSurvey();

	// publiczna metoda
	public static GenerateIdSurvey getInstance() {

		return generator;
	}
	/**
	 * Generowanie identyfikatora dla ankiety
	 * @return identyfikator ankiety
	 */
	public synchronized int generateSurveyID() {

		return ++ankietaid;
	}
	/**
	 * Generowanie identyfikatora dla pytania ankiety
	 * @return identyfikator pytania
	 */
	public synchronized int generateSurveyQuestionID() {

		return ++pytanieankietyid;
	}
}
