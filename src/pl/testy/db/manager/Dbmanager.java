package pl.testy.db.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * Klasa obsługująca połaczenie z bazą danych
 * @author wojtek
 *
 */
public class Dbmanager {
	public static final String PASSWORD = "testy";
	public static final String USER = "root";
	public static  String DATA_BASE = "jdbc:mysql://localhost/testdb?characterEncoding=UTF-8";
/**
 * Rozpoczęcie połączenia z bazą danych
 * @return obiekt Connection
 */
	public Connection startConnections() {

		try {
			return DriverManager.getConnection(DATA_BASE, USER, PASSWORD);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return null;
		}
		

	}

}
