package pl.testy.db.manager;



import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pl.testy.model.Odpowiedz;
import pl.testy.model.Pytanie;
import pl.testy.model.TestHeader;
import pl.testy.model.Wynik;
/**
 * Klasa obsługująca wymiane informacji z bazą danych dla testów i informacji z nimi związanych
 * @author wojtek
 *
 */
public class TestDbManager extends Dbmanager {
/**
 * Zapisywanie testu
 * @param tytul nazwa
 * @param opis opis
 * @param login login użytkownika tworzącego
 * @param listapytan lista pytań
 * @param punkty maksymalna możliwa ilość punktów
 * @param czastrwania czas trwania testu
 */
	public void saveTest(String tytul, String opis, String login,
			List<Pytanie> listapytan,int punkty,int czastrwania) {

		UserDbManager userDbManager = new UserDbManager();
		GenerateId generator = GenerateId.getInstance(); // odniesienie do
															// stworzonego
															// obiektu

		try {

			int iddydaktyk = userDbManager.getIdByLogin(login);
			int idtestu = generator.generateTestID();
			System.out.println(".....");
			System.out.println(tytul);
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("INSERT INTO test (nazwa_testu, opis_testu, stan, id_dydaktyka, id_test, max_punkty, czas_trwania ) VALUES (?,?,'N',?,?,?,?);");

			statement.setString(1, tytul);
			statement.setString(2, opis);
			statement.setInt(3, iddydaktyk);
			statement.setInt(4, idtestu);
			statement.setInt(5, punkty);
			statement.setInt(6, czastrwania);
			statement.execute();

			for (Pytanie pytanie : listapytan) {

				int idpytanie = generator.generatePytanieID();

				PreparedStatement statement2 = connect
						.prepareStatement("INSERT INTO pytania(id_pytanie, id_test,nazwa_pytania, foto,video) VALUES (?,?,?,?,?);");
				statement2.setInt(1, idpytanie);
				statement2.setInt(2, idtestu);
				statement2.setString(3, pytanie.getTresc());
				statement2.setString(4, pytanie.getFoto());
				statement2.setString(5, pytanie.getVideo());
				statement2.execute();

				for (Odpowiedz odpowiedz : pytanie.getListaodpowiedz()) {

					PreparedStatement statement3 = connect
							.prepareStatement("INSERT INTO odpowiedz(id_pytanie,tresc_odpowiedzi,ilosc_punktow) VALUES (?,?,?);");
					statement3.setInt(1, idpytanie);
					statement3.setString(2, odpowiedz.getTresc());
					statement3.setString(3, odpowiedz.getPunktacja());
					statement3.execute();
				}

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
/**
 * Lista testów dydaktyka
 * @param logindydaktyka login dydaktyka
 * @return lista obiektów TestHeader
 */
	public List<TestHeader> testlist(String logindydaktyka) {

		List<TestHeader> testlist = new ArrayList<TestHeader>();

		try {
			Connection connect = startConnections();

			UserDbManager mgm = new UserDbManager();

			int iddydaktyka = mgm.getIdByLogin(logindydaktyka);

			PreparedStatement statement = connect
					.prepareStatement("SELECT t.id_test, t.nazwa_testu, t.opis_testu, t.stan, t.czas_trwania FROM test t WHERE t.id_dydaktyka =? AND NOT t.stan='AR' ;");
			statement.setInt(1, iddydaktyka);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				TestHeader th = new TestHeader();

				th.setCzastrwania(wynik.getInt("czas_trwania"));
				th.setNazwa(wynik.getString("nazwa_testu"));
				th.setOpis(wynik.getString("opis_testu"));
				th.setStan(wynik.getString("stan"));
				th.setIdtestu(wynik.getInt("id_test"));
				testlist.add(th); // dorzucanie do listy
			}
			statement.close();
			wynik.close();
			connect.close();
			return testlist;

		} catch (SQLException e) {
			e.printStackTrace();
			return testlist;
		}
	}
	/**
	 * Sprawdzanie uprawnień do wyświetlania wyników
	 * @param idtester identyfikator testera
	 * @param idwyniku identyfikator wyniku
	 * @return wartość logiczna true dla uprawnionych
	 */
	public boolean checkResultPermission(int idtester, int idwyniku) {

		boolean nazwajest = false;
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT stan FROM wyniki WHERE id_wyniki=? AND id_testera=?;");
			statement.setInt(1, idwyniku);
			statement.setInt(2, idtester);

			ResultSet wynik = statement.executeQuery();
			if (wynik.next()) {
				nazwajest = true;
			}
			statement.close();
			connect.close();
			return nazwajest;
		} catch (SQLException e) {
			e.printStackTrace();
			return nazwajest;
		}

	}
	/**
	 * Sprawdzanie uprawnień do wyświetlania testu
	 * @param login login uzytkownika
	 * @param idtestu identyfikator testu
	 * @return wartość logiczna true dla uprawnionych
	 */
	public boolean checkTestPermission(String login, int idtestu) {

		boolean nazwajest = false;
		UserDbManager udm = new UserDbManager();
		
		int iddydaktyka = udm.getIdDydaktykByTesterLogin(login);
		
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT nazwa_testu FROM test WHERE id_test=? AND id_dydaktyka=?;");
			statement.setInt(1, idtestu);
			statement.setInt(2, iddydaktyka);

			ResultSet wynik = statement.executeQuery();
			if (wynik.next()) {
				nazwajest = true;
			}
			statement.close();
			connect.close();
			return nazwajest;
		} catch (SQLException e) {
			e.printStackTrace();
			return nazwajest;
		}

	}
	/**
	 * Pobieranie listy testów archiwum dla dydaktyka
	 * @param logindydaktyka login dydaktyka
	 * @return lista obiektów TestHeader
	 */
	public List<TestHeader> testListArchieve(String logindydaktyka) {

		List<TestHeader> testlistarchiwum = new ArrayList<TestHeader>();

		try {
			Connection connect = startConnections();

			UserDbManager mgm = new UserDbManager();

			int iddydaktyka = mgm.getIdByLogin(logindydaktyka);

			PreparedStatement statement = connect
					.prepareStatement("SELECT t.id_test, t.nazwa_testu, t.opis_testu, t.stan, t.czas_trwania FROM test t WHERE t.id_dydaktyka =? AND t.stan='AR' ");
			statement.setInt(1, iddydaktyka);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				TestHeader ta = new TestHeader();

				ta.setCzastrwania(wynik.getInt("czas_trwania"));
				ta.setNazwa(wynik.getString("nazwa_testu"));
				ta.setOpis(wynik.getString("opis_testu"));
				ta.setStan(wynik.getString("stan"));
				ta.setIdtestu(wynik.getInt("id_test"));
				testlistarchiwum.add(ta); // dorzucanie do listy
			}
			statement.close();
			wynik.close();
			connect.close();
			return testlistarchiwum;

		} catch (SQLException e) {
			e.printStackTrace();
			return testlistarchiwum;
		}
	}
	/**
	 *  Pobieranie listy testów archiwum dla dydaktyka
	 * @param logintestera login testera
	 * @return lista obiektów TestHeader
	 */
	public List<TestHeader> testlistuser(String logintestera) {

		List<TestHeader> testlist = new ArrayList<TestHeader>();

		try {
			Connection connect = startConnections();

			UserDbManager mgm = new UserDbManager();

			int dydaktykid = mgm.getIdDydaktykByTesterLogin(logintestera);
			int idTester = mgm.getIdByLogin(logintestera);

			
			PreparedStatement statement = connect
					.prepareStatement("SELECT t.id_test, t.nazwa_testu, t.opis_testu, t.stan, t.czas_trwania FROM test t WHERE t.id_dydaktyka =? AND stan='A' and not EXISTS (select * from wyniki w where t.id_test=w.id_test and id_testera = ?);");
			statement.setInt(1, dydaktykid);
			statement.setInt(2, idTester);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				TestHeader th = new TestHeader();

				th.setCzastrwania(wynik.getInt("czas_trwania"));
				th.setNazwa(wynik.getString("nazwa_testu"));
				th.setOpis(wynik.getString("opis_testu"));
				th.setStan(wynik.getString("stan"));
				th.setIdtestu(wynik.getInt("id_test"));
				testlist.add(th); // dorzucanie do listy
			}
			System.out.println("///////");
			System.out.println(testlist);
			statement.close();
			wynik.close();
			connect.close();
			return testlist;

		} catch (SQLException e) {
			e.printStackTrace();
			return testlist;
		}
	}
	/**
	 * Pobieranie informacji o teście
	 * @param idtestu identyfikator testu
	 * @return  Obiekt TestHeader
	 */
	public TestHeader showTest(int idtestu) {

		TestHeader th = new TestHeader();

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT id_test, nazwa_testu, stan, max_punkty, czas_trwania,opis_testu FROM test WHERE id_test =? ;");
			statement.setInt(1, idtestu);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			if (wynik.next()) { // przeszukiwanie wierszy

				

				th.setCzastrwania(wynik.getInt("czas_trwania"));
				th.setNazwa(wynik.getString("nazwa_testu"));
				th.setOpis(wynik.getString("opis_testu"));
				th.setStan(wynik.getString("stan"));
				th.setIdtestu(wynik.getInt("id_test"));
				th.setMaxpunkty(wynik.getInt("max_punkty"));
			}

			statement.close();
			wynik.close();
			connect.close();
			return th;

		} catch (SQLException e) {
			e.printStackTrace();
			return th;
		}
	}
	/**
	 * Uaktualnienie danych testu
	 * @param idtest identyfikator testu
	 * @param nazwa nazwa testu
	 * @param opis opis testu
	 * @param stan stan testu
	 * @param czastrwania czas trwania testu
	 * @param maxpunty maksymalna ilośc punktów testu
	 */
	public void updateTest(int idtest, String nazwa, String opis,String stan,int czastrwania,int maxpunty) {
		try {

			
			
			
			
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("UPDATE test SET nazwa_testu=?, opis_testu=?, stan=?, czas_trwania=?, max_punkty=?  WHERE id_test=? ;");
			statement.setString(1, nazwa);
			statement.setString(2, opis);
			statement.setString(3, stan);
			statement.setInt(4, czastrwania);
			statement.setInt(5, maxpunty);
			statement.setInt(6, idtest);
			statement.execute();
			statement.close();
			connect.close();
			
		} catch (SQLException e) {
			e.printStackTrace();// TODO: handle exception
		}
	}
	/**
	 * Pobieranie listy wyników danego testu
	 * @param idtestu identyfikator testu
	 * @return Lista obiektów Wynik
	 */
	public List<Wynik> wynikList(int idtestu) {

		List<Wynik> testlist = new ArrayList<Wynik>();

		try {
			Connection connect = startConnections();

		

			

			PreparedStatement statement = connect
					.prepareStatement("SELECT t.nazwa_testu, t.id_test, u.imie, u.nazwisko, w.zdobyty_wynik, w.data_start, w.data_stop FROM test t, users u, wyniki w WHERE w.id_testera=u.id_users AND t.id_test=? AND w.id_test=t.id_test ORDER BY w.data_start;");
			statement.setInt(1, idtestu);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				Wynik th = new Wynik();

		th.setIdtestu(idtestu);
		th.setNazwatestu(wynik.getString("t.nazwa_testu"));
		th.setImie(wynik.getString("u.imie"));
		th.setNazwisko(wynik.getString("u.nazwisko"));
		th.setZdobytywynik(wynik.getInt("w.zdobyty_wynik"));
		th.setDatastart(wynik.getDate("w.data_start"));
		th.setDatastop(wynik.getDate("w.data_stop"));
		
				testlist.add(th); // dorzucanie do listy
			}
			statement.close();
			wynik.close();
			connect.close();
			return testlist;

		} catch (SQLException e) {
			e.printStackTrace();
			return testlist;
		}
	}
	/**
	 * Pobieranie listy wyników testu danego użytkownika 
	 * @param iduser identyfikator użytkownika
	 * @return Lista obiektów Wynik
	 */
	public List<Wynik> wynikListUser(int iduser) {

		List<Wynik> wyniklist = new ArrayList<Wynik>();

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT t.nazwa_testu, t.id_test, t.max_punkty, w.zdobyty_wynik, w.data_start, w.data_stop, w.id_wyniki FROM test t, wyniki w WHERE w.id_testera=?  AND w.id_test=t.id_test AND w.stan='A' ORDER BY w.data_start;");
			statement.setInt(1, iduser);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				Wynik w = new Wynik();

		w.setIdtestu(wynik.getInt("t.id_test"));
		w.setIdwyniku(wynik.getInt("w.id_wyniki"));
		w.setMaxpunkty(wynik.getInt("t.max_punkty"));
		w.setNazwatestu(wynik.getString("t.nazwa_testu"));
		w.setZdobytywynik(wynik.getInt("w.zdobyty_wynik"));
		w.setDatastart(wynik.getDate("w.data_start"));
		w.setDatastop(wynik.getDate("w.data_stop"));
		
		wyniklist.add(w); // dorzucanie do listy
			}
			statement.close();
			wynik.close();
			connect.close();
			return wyniklist;

		} catch (SQLException e) {
			e.printStackTrace();
			return wyniklist;
		}
	}
	/**
	 * Pobieranie listy wyników testów dla uzytkownika przeniesionych do archiwum
	 * @param iduser identyfikator użytkownika
	 * @return Lista obiektów Wynik
	 */
	public List<Wynik> wynikListUserArchieve(int iduser) {

		List<Wynik> wyniklist = new ArrayList<Wynik>();

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT t.nazwa_testu, t.id_test, t.max_punkty, w.zdobyty_wynik, w.data_start, w.data_stop, w.id_wyniki FROM test t, wyniki w WHERE w.id_testera=?  AND w.id_test=t.id_test AND w.stan='AR' ORDER BY w.data_start;");
			statement.setInt(1, iduser);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				Wynik w = new Wynik();

		w.setIdtestu(wynik.getInt("t.id_test"));
		w.setIdwyniku(wynik.getInt("w.id_wyniki"));
		w.setMaxpunkty(wynik.getInt("t.max_punkty"));
		w.setNazwatestu(wynik.getString("t.nazwa_testu"));
		w.setZdobytywynik(wynik.getInt("w.zdobyty_wynik"));
		w.setDatastart(wynik.getDate("w.data_start"));
		w.setDatastop(wynik.getDate("w.data_stop"));
		
		wyniklist.add(w); // dorzucanie do listy
			}
			statement.close();
			wynik.close();
			connect.close();
			return wyniklist;

		} catch (SQLException e) {
			e.printStackTrace();
			return wyniklist;
		}
	}
	/**
	 * Zapisywanie danych w momencie rozpoczęcia testu
	 * @param idtestu identyfikator testu
	 * @param idtestera identyfikator użytkownika
	 */
	public void starttest(int idtestu, int idtestera) {

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("INSERT INTO wyniki (id_test,id_testera,zdobyty_wynik) VALUES(?,?,0);");
			statement.setInt(1, idtestu);
			statement.setInt(2, idtestera);
			statement.execute();
			statement.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();

		}

	}
/**
 * Pobieranie informacji o teście
 * @param id identyfikator testu
 * @return obiekt TestHeader
 */
	public TestHeader getTestById(int id) {
		TestHeader th = new TestHeader();
		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT t.id_test,t.nazwa_testu, t.opis_testu, t.czas_trwania,(SELECT COUNT(*) FROM pytania p WHERE p.id_test=t.id_test) iloscpytan FROM test t WHERE t.id_test=?;");
			statement.setInt(1, id);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			if (wynik.next()) { // przeszukiwanie wierszy

				th.setCzastrwania(wynik.getInt("czas_trwania"));
				th.setNazwa(wynik.getString("nazwa_testu"));
				th.setOpis(wynik.getString("opis_testu"));

				th.setIdtestu(wynik.getInt("t.id_test"));
				th.setIloscpytantestu(wynik.getInt("iloscpytan"));
			}

			statement.close();
			wynik.close();
			connect.close();
			return th;

		} catch (SQLException e) {
			e.printStackTrace();
			return th;
		}
	}
/**
 * Pobieranie informacji o pytaniu i jego odpowiedziach
 * @param idpytania identyfikator pytania 
 * @return obiekt Pytanie
 */
	 public Pytanie getPytanie(int idpytania) {
		  Pytanie p = new Pytanie();
		  try {
		   Connection connect = startConnections();

		   PreparedStatement statement = connect
		     .prepareStatement("SELECT p.nazwa_pytania,p.foto,p.video,o.tresc_odpowiedzi,o.ilosc_punktow,o.id_odpowiedz FROM pytania p, odpowiedz o  WHERE p.id_pytanie = o.id_pytanie AND p.id_pytanie=?;");
		   statement.setInt(1, idpytania);
		   ResultSet wynik = statement.executeQuery(); // result set zwraca
		              // dwuwymiarowa tablice
		   List<Odpowiedz> listao = new ArrayList<Odpowiedz>();

		   if (wynik.next()) { // przeszukiwanie wierszy

		    p.setTresc(wynik.getString("p.nazwa_pytania"));
		    p.setFoto(wynik.getString("p.foto"));
		    p.setVideo(wynik.getString("p.video"));
		    Odpowiedz o = new Odpowiedz();
		    o.setTresc(wynik.getString("tresc_odpowiedzi"));
		    o.setPunktacja(wynik.getString("ilosc_punktow"));
		    o.setIdodpowiedzi(wynik.getInt("o.id_odpowiedz"));
		    listao.add(o);
		   }

		   while (wynik.next()) { // przeszukiwanie wierszy

		    Odpowiedz o = new Odpowiedz();
		    o.setTresc(wynik.getString("tresc_odpowiedzi"));
		    o.setPunktacja(wynik.getString("ilosc_punktow"));
		    o.setIdodpowiedzi(wynik.getInt("o.id_odpowiedz"));
		    listao.add(o);
		   }
		   p.setListaodpowiedz(listao);
		   statement.close();
		   wynik.close();
		   connect.close();
		   return p;

		  } catch (SQLException e) {
		   e.printStackTrace();
		   return p;
		  }
		 }
/**
 * Pobieranie identyfikatorów pytań danego testu
 * @param idtestu identyfikator testu
 * @return Lista identyfikatorów pytań
 */
	public List<Integer> getIdPytan(int idtestu) {
		List<Integer> listaidpytan = new ArrayList<Integer>();
		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT p.id_pytanie FROM pytania p  WHERE p.id_test = ?;");
			statement.setInt(1, idtestu);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy
				listaidpytan.add(wynik.getInt("p.id_pytanie"));

			}

			statement.close();
			wynik.close();
			connect.close();
			return listaidpytan;

		} catch (SQLException e) {
			e.printStackTrace();
			return listaidpytan;
		}
	}
	/**
	 * Aktualizacja stanu wyniku na archiwalnyc
	 * @param idtestera identyfikator użytkownika tester
	 * @param idwyniku identyfikator wyniku
	 */
	public void addResultToArchieve(int idtestera, int idwyniku) {

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("update wyniki set stan = 'AR' WHERE id_testera = ? AND id_wyniki = ?;");
		
			statement.setInt(1, idtestera);
			statement.setInt(2, idwyniku);
			statement.executeUpdate(); // result set zwraca

			statement.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Usuwanie wyniku z listy archiwum
	 * @param idtestera
	 * @param idwyniku
	 */
	public void deleteResultFromArchieve(int idtestera, int idwyniku) {

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("update wyniki set stan = 'D' WHERE id_testera = ? AND id_wyniki = ?;");
		
			statement.setInt(1, idtestera);
			statement.setInt(2, idwyniku);
			statement.executeUpdate(); // result set zwraca

			statement.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Pobieranie wyników odpowiedzi
	 * @param odpList lista identyfikatorów odpowiedzi
	 * @return ilość zdobytych punktów
	 */
	public int getWynikOdpowiedzi(List<Integer> odpList) {
		int sumaPunktow = 0;
		try {

			Connection connect = startConnections();

			String where = "";
			if (odpList.size() != 0) {
				where += "id_odpowiedz = ?";
			}

			for (int i = 1; i < odpList.size(); i++) {
				where += " OR id_odpowiedz = ?";
			}

			PreparedStatement statement = connect
					.prepareStatement("select sum(ilosc_punktow) punkty from odpowiedz where "
							+ where);

			for (int i = 0; i < odpList.size(); i++) {
				statement.setInt(i + 1, odpList.get(i));
			}

			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			if (wynik.next()) { // przeszukiwanie wierszy

				sumaPunktow = wynik.getInt("punkty");

			}

			statement.close();
			wynik.close();
			connect.close();
			return sumaPunktow;

		} catch (SQLException e) {
			e.printStackTrace();
			return sumaPunktow;
		}
	}

	/**
	 * Uaktualnienie wyniku testu podczas zakończenia
	 * @param login login użytkownika
	 * @param testId identyfikator testu
	 * @param wynikPunktow zdobyty wynik
	 */
	public void updateWynik(String login, int testId, int wynikPunktow) {

		try {
			Connection connect = startConnections();

			UserDbManager mgm = new UserDbManager();

			int userId = mgm.getIdByLogin(login);

			PreparedStatement statement = connect
					.prepareStatement("update wyniki set zdobyty_wynik = ?, data_stop = now() WHERE id_testera = ? AND id_test = ?;");
			statement.setInt(1, wynikPunktow);
			statement.setInt(2, userId);
			statement.setInt(3, testId);
			statement.executeUpdate(); // result set zwraca

			statement.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Pobieranie daty rozpoczęcia testu
	 * @param idtest identyfikator testu
	 * @param idtestera identyfikator testera
	 * @return data rozpoczęcia testu
	 */
	public Date getStartTest(int idtest, int idtestera) {

		Date datastart = null;
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT data_start FROM wyniki WHERE id_test=? AND id_testera=? ;");
			statement.setInt(1, idtest);
			statement.setInt(2, idtestera);
			ResultSet wynik = statement.executeQuery();

			if (wynik.next()) {

				datastart = new Date(wynik.getTimestamp("data_start").getTime());
			}

			return datastart;
		} catch (Exception e) {
			return datastart;// TODO: handle exception
		}
	}
/**
 * Pobieranie daty zakończenia testu
 * @param idtest identyfikator testu
 * @param idtestera identyfikator testera
 * @return data zakończenia testu
 */
	public Date getStopTest(int idtest, int idtestera) {

		Date datastart = null;
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT data_stop FROM wyniki WHERE id_test=? AND id_testera=? ;");
			statement.setInt(1, idtest);
			statement.setInt(2, idtestera);
			ResultSet wynik = statement.executeQuery();

			if (wynik.next()) {

				datastart = new Date(wynik.getTimestamp("data_stop").getTime());
			}

			return datastart;
		} catch (Exception e) {
			return datastart;// TODO: handle exception
		}
	}
	/**
	 * Pobieranie czasu trwania testu
	 * @param idtest identyfikator testu
	 * @return czas trwania testu
	 */
public int getTestDuration(int idtest) {
	
	int czastrwania = 0;
	try {
		
		Connection connect = startConnections();
		PreparedStatement statement = connect
				.prepareStatement("SELECT czas_trwania FROM test WHERE id_test=? ;");
		statement.setInt(1, idtest);
		ResultSet wynik = statement.executeQuery();
		
		if (wynik.next()){
			
			czastrwania = wynik.getInt("czas_trwania");
		}
		
		
		return czastrwania;
	} catch (Exception e) {
		return czastrwania;// TODO: handle exception
	}
	
	
}

}
