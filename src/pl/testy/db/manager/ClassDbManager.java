package pl.testy.db.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pl.testy.model.ClassFull;
import pl.testy.model.ClassHeader;
import pl.testy.model.Grupa;

/**
 * Klasa obsługująca wymiane informacji z bazą danych dla klas i grup użytkowników
 * @author wojtek
 *
 */
public class ClassDbManager extends Dbmanager {

	/**
	 * Metoda sprawdzająca czy nazwa danej klasy istnieje w bazie danych dla danego użytkownika
	 * @param nazwa nazwa klasy
	 * @param logindydaktyka login użytkownika
	 * @return wartość logiczną true dla istniejącej nazwy
	 */
	public boolean checkClass(String nazwa, String logindydaktyka) {

		UserDbManager mgm = new UserDbManager();
		int iddydaktyka = mgm.getIdByLogin(logindydaktyka);
		
		boolean nazwajest = false;
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT nazwa FROM klasy k WHERE k.nazwa=? AND k.id_dydaktyka=?;");
			statement.setString(1, nazwa);
			statement.setInt(2, iddydaktyka);

			ResultSet wynik = statement.executeQuery();
			if (wynik.next()) {
				nazwajest = true;
			}
			statement.close();
			connect.close();
			return nazwajest;
		} catch (SQLException e) {
			e.printStackTrace();
			return nazwajest;
		}

	}
	/**
	 * Metoda sprawdzająca czy nazwa dla danej grupy istnieje już w danej klasie
	 * @param nazwa nazwa grupy
	 * @param idklasy identyfikator klasy
	 * @return wartość logiczna true dla istniejącej nazwy
	 */
	public boolean checkGroup(String nazwa, int idklasy) {

		boolean nazwajest = false;
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT g.nazwa FROM grupy g, klasy k WHERE g.nazwa=? AND k.id_klasy=?;");
			statement.setString(1, nazwa);
			statement.setInt(2, idklasy);

			ResultSet wynik = statement.executeQuery();
			if (wynik.next()) {
				nazwajest = true;
			}
			statement.close();
			connect.close();
			return nazwajest;
		} catch (SQLException e) {
			e.printStackTrace();
			return nazwajest;
		}

	}
/**
 * Zapisywanie klasy do bazy danych
 * @param nazwa nazwa klasy
 * @param login login użytkownika 
 * @param listagrup lista grup wchodzących w skład klasy
 */
	public void saveClass(String nazwa, String login, List<Grupa> listagrup) {

		UserDbManager userDbManager = new UserDbManager();

		try {

			int iddydaktyk = userDbManager.getIdByLogin(login);

			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("INSERT INTO klasy (id_dydaktyka, nazwa) VALUES (?,?);");

			statement.setInt(1, iddydaktyk);
			statement.setString(2, nazwa);
			statement.execute();
			statement.close();

			PreparedStatement statement2 = connect
					.prepareStatement("SELECT id_klasy FROM klasy WHERE id_dydaktyka=? AND nazwa=?;");
			statement2.setInt(1, iddydaktyk);
			statement2.setString(2, nazwa);
			ResultSet wynik = statement2.executeQuery(); // result set zwraca
															// dwuwymiarowa
															// tablice

			int id = 0;

			if (wynik.next()) { // przeszukiwanie wierszy

				id = wynik.getInt("id_klasy"); // jezeli cos znajdzie to zwroci
												// "id_klasy"

			}
			statement2.close();
			wynik.close();

			for (Grupa grupa : listagrup) {

				PreparedStatement statement3 = connect
						.prepareStatement("INSERT INTO grupy (id_klasy, nazwa) VALUES (?,?);");
				statement3.setInt(1, id);
				statement3.setString(2, grupa.getNazwagrupy());

				statement3.execute();
				statement3.close();

			}
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
/**
 * Zapisywanie domyślnej klasy podczas rejestracji dydaktyka
 * @param login login uzytkownika
 */
	public void saveDefaultClass(String login) {

		UserDbManager userDbManager = new UserDbManager();

		try {

			int iddydaktyk = userDbManager.getIdByLogin(login);
			String nazwa = "Klasa1";
			String grupa = "Grupa wszyscy";

			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("INSERT INTO klasy (id_dydaktyka, nazwa) VALUES (?,?);");

			statement.setInt(1, iddydaktyk);
			statement.setString(2, nazwa);
			statement.execute();
			statement.close();

			PreparedStatement statement2 = connect
					.prepareStatement("SELECT id_klasy FROM klasy WHERE id_dydaktyka=? AND nazwa=?;");
			statement2.setInt(1, iddydaktyk);
			statement2.setString(2, nazwa);
			ResultSet wynik = statement2.executeQuery(); // result set zwraca
															// dwuwymiarowa
															// tablice

			int id = 0;

			if (wynik.next()) { // przeszukiwanie wierszy

				id = wynik.getInt("id_klasy"); // jezeli cos znajdzie to zwroci
												// "id_klasy"

			}
			statement2.close();
			wynik.close();

			PreparedStatement statement3 = connect
					.prepareStatement("INSERT INTO grupy (id_klasy, nazwa) VALUES (?,?);");
			statement3.setInt(1, id);
			statement3.setString(2, grupa);

			statement3.execute();
			statement3.close();

			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
/**
 * Edycja istniejącej klasy
 * @param nazwa dotychczasowa nazwa klasy
 * @param nowanazwa nowa nazwa klasy
 * @param login login użytkownika
 */
	public void updateClass(String nazwa, String nowanazwa, String login) {

		UserDbManager userDbManager = new UserDbManager();

		try {

			int iddydaktyk = userDbManager.getIdByLogin(login);

			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("UPDATE klasy SET nazwa=? WHERE nazwa=? AND id_dydaktyka=? ;");

			statement.setString(1, nowanazwa);
			statement.setString(2, nazwa);
			statement.setInt(3, iddydaktyk);
			statement.execute();
			statement.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
/**
 * Tworzenie listy klas danego uzytkownika
 * @param logindydaktyka login użytkownika
 * @return lista obiektów ClassHeader
 */
	public List<ClassHeader> classlist(String logindydaktyka) {

		List<ClassHeader> classlist = new ArrayList<ClassHeader>();

		try {
			Connection connect = startConnections();

			UserDbManager mgm = new UserDbManager();
			int iddydaktyka = mgm.getIdByLogin(logindydaktyka);

			PreparedStatement statement = connect
					.prepareStatement("SELECT k.id_klasy, k.nazwa FROM klasy k  WHERE k.id_dydaktyka =?;");
			statement.setInt(1, iddydaktyka);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			while (wynik.next()) { // przeszukiwanie wierszy

				ClassHeader ch = new ClassHeader();
				ch.setIdklasy(wynik.getInt("id_klasy"));
				ch.setNazwaklasy(wynik.getString("nazwa"));

				classlist.add(ch); // dorzucanie do listy
			}
			statement.close();
			wynik.close();
			connect.close();
			return classlist;

		} catch (SQLException e) {
			e.printStackTrace();
			return classlist;
		}
	}
/**
 * Wybranie wszystkich informacji o klasie z bazy danych
 * @param id identyfikator klasy
 * @return obiekt ClassFull
 */
	public ClassFull getClassById(int id)

	{

		ClassFull cf = new ClassFull();

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT  k.nazwa,  g.nazwa, g.id_klasy, g.id_grupy  FROM klasy k, grupy g WHERE k.id_klasy=? AND g.id_klasy=?;");

			statement.setInt(1, id);
			statement.setInt(2, id);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

			List<Grupa> listagrup = new ArrayList<Grupa>();
			if (wynik.next()) {
				cf.setIdklasy(id);
				cf.setNazwaklasy(wynik.getString("k.nazwa"));

				Grupa grupa = new Grupa();
				grupa.setNazwagrupy(wynik.getString("g.nazwa"));
				grupa.setIdKlasygrupy(wynik.getInt("g.id_klasy"));
				grupa.setIdgrupysql(wynik.getInt("g.id_grupy"));
				listagrup.add(grupa);
			}

			while (wynik.next()) { // przeszukiwanie wierszy

				Grupa grupa = new Grupa();
				grupa.setNazwagrupy(wynik.getString("g.nazwa"));
				grupa.setIdKlasygrupy(wynik.getInt("g.id_klasy"));
				grupa.setIdgrupysql(wynik.getInt("g.id_grupy"));
				listagrup.add(grupa);

			}

			cf.setListagrup(listagrup);

			statement.close();
			wynik.close();
			connect.close();
			return cf;

		} catch (SQLException e) {
			e.printStackTrace();
			return cf;

		}
	}
/**
 * Pobieranie identyfikatora klasy
 * @param nazwaklasy nazwa klasy
 * @param iddydaktyka identyfikator użytkownika
 * @return identyfikator klasy
 */
	public int getIdClassByName(String nazwaklasy, int iddydaktyka)

	{

		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT  id_klasy FROM klasy  WHERE nazwa=? AND id_dydaktyka=?;");

			statement.setString(1, nazwaklasy);
			statement.setInt(2, iddydaktyka);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice
			int idklasy;
			idklasy = 0;
			if (wynik.next()) {
				idklasy = wynik.getInt("id_klasy");
			}

			statement.close();
			wynik.close();
			connect.close();
			return idklasy;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		}
	}
/**
 * Pobieranie identyfikatora grupy
 * @param nazwagrupy nazwa grupy
 * @param idklasy identyfikator klasy
 * @return identyfikator grupy
 */
	public int getIdGroupByName(String nazwagrupy, int idklasy)

	{

		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT  id_grupy FROM grupy  WHERE nazwa=? AND id_klasy=?;");

			statement.setString(1, nazwagrupy);
			statement.setInt(2, idklasy);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice
			int idgrupy = 0;

			if (wynik.next()) {
				idgrupy = wynik.getInt("id_grupy");
			}

			statement.close();
			wynik.close();
			connect.close();
			return idgrupy;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;

		}
	}
	/**
	 * Pobieranie wszystkich danych o grupie
	 * @param id identyfikator grupy
	 * @return obiekt Grupa
	 */
	public Grupa getGroupById(int id)

	{

		Grupa g = new Grupa();

		try {
			Connection connect = startConnections();

			PreparedStatement statement = connect
					.prepareStatement("SELECT g.id_grupy, g.id_klasy, g.nazwa FROM grupy g WHERE g.id_grupy=?;");

			statement.setInt(1, id);
			ResultSet wynik = statement.executeQuery(); // result set zwraca
														// dwuwymiarowa tablice

	
			if (wynik.next()) {

				
				g.setNazwagrupy(wynik.getString("g.nazwa"));
				g.setIdKlasygrupy(wynik.getInt("g.id_klasy"));
				g.setIdgrupysql(wynik.getInt("g.id_grupy"));
				
			}



			

			statement.close();
			wynik.close();
			connect.close();
			return g;

		} catch (SQLException e) {
			e.printStackTrace();
			return g;

		}
	}
	/**
	 * Uaktualnienie danych o grupie
	 * @param nazwa nowa nazwa grupy 
	 * @param idgrupy identyfikator grupy
	 */
	public void updateGroupName(String nazwa, int idgrupy) {

		try {

			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("UPDATE grupy SET nazwa=? WHERE id_grupy=? ;");

			statement.setString(1, nazwa);
			statement.setInt(2, idgrupy);
			statement.execute();
			statement.close();
			connect.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Walidacja dostępu do klasy
	 * @param idklasy identyfikator klasy
	 * @param logindydaktyka login użytkownika
	 * @return wartość logiczna true dla istniejącej klasy danego użytkownika
	 */
	public boolean checkClassPermission(int idklasy, int logindydaktyka) {

		boolean nazwajest = false;
		try {
			Connection connect = startConnections();
			PreparedStatement statement = connect
					.prepareStatement("SELECT nazwa FROM klasy k WHERE k.id_klasy=? AND k.id_dydaktyka=?;");
			statement.setInt(1, idklasy);
			statement.setInt(2, logindydaktyka);

			ResultSet wynik = statement.executeQuery();
			if (wynik.next()) {
				nazwajest = true;
			}
			statement.close();
			connect.close();
			return nazwajest;
		} catch (SQLException e) {
			e.printStackTrace();
			return nazwajest;
		}

	}
	/**
	 * Usunięcie grupy
	 * @param idgrupy identyfikator grupy
	 */
public void deleteGroup(int idgrupy) {
	try {

		Connection connect = startConnections();
		PreparedStatement statement = connect
				.prepareStatement("DELETE FROM grupy WHERE id_grupy=? ;");

		
		statement.setInt(1, idgrupy);
		statement.execute();
		statement.close();
		connect.close();

	} catch (SQLException e) {
		e.printStackTrace();
	}
  }
/**
 * Usunięcie klasy
 * @param idklasy identyfikator klasy
 */
public void deleteClass(int idklasy) {
	try {

		Connection connect = startConnections();
		PreparedStatement statement = connect
				.prepareStatement("DELETE FROM klasy WHERE id_klasy=? ;");

		
		statement.setInt(1, idklasy);
		statement.execute();
		statement.close();
		connect.close();

	} catch (SQLException e) {
		e.printStackTrace();
	}
  }
/**
 * Zapisywanie grupy
 * @param nazwa nazwa grupy
 * @param idklasy identyfikator klasy
 */
public void saveGroup(String nazwa, int idklasy) {

	try {

		Connection connect = startConnections();
		PreparedStatement statement = connect
				.prepareStatement("INSERT INTO grupy (id_klasy, nazwa) VALUES (?,?);");

		statement.setInt(1, idklasy);
		statement.setString(2, nazwa);
		statement.execute();
		statement.close();
		connect.close();

	} catch (SQLException e) {
		e.printStackTrace();
	}
}
}


