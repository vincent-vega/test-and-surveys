package pl.testy.db.manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Klasa generująca identyfikatory dla testu i pytań zawiera prywatny konstruktor oparta na wzorcu singleton
 * @author wojtek
 *
 */
public class GenerateId extends Dbmanager {

	// prywatny konstruktor to jest wykorzystywane w klasie w ktorej jest
	private GenerateId() {

		try {

			Connection connect = startConnections();
			PreparedStatement statement = connect.prepareStatement("SELECT max(id_test) FROM test;");
			ResultSet wynik = statement.executeQuery();
			
			if (wynik.next()){
				testid = wynik.getInt(1);
			} 
			statement = connect.prepareStatement("SELECT max(id_pytanie) FROM pytania;");
			 wynik = statement.executeQuery();
			
			if (wynik.next()){
				pytanieid = wynik.getInt(1);
			}
			statement.close();
			wynik.close();
			connect.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private int testid;
	private int pytanieid;

	// prywatne statyczne pole
	private static GenerateId generator = new GenerateId();

	// publiczna metoda

	public static GenerateId getInstance() {

		return generator;
	}
/**
 * Generowanie identyfikatora dla testu
 * @return identyfikator testu
 */
	public synchronized int generateTestID() {

		return ++testid;
	}
/**
 * Generowanie identyfiaktora dla pytania
 * @return identyfikator pytania
 */
	public synchronized int generatePytanieID() {

		return ++pytanieid;
	}
}
