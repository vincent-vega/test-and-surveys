package pl.testy.service;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
/**
 * Klasa obsługująca wysyłanie poczty elektronicznej
 * @author wojtek
 *
 */
public class MailService {

	private static final String HOST = "smtp.gmail.com";
	private static final int PORT = 465;
	// Adres email osby która wysyła maila
	private static final String FROM = "testy.ankiety@gmail.com";
	// Hasło do konta osoby która wysyła maila
	private static final String PASSWORD = "testyiankiety";

	// Adres email osoby do której wysyłany jest mail
/**
 * Metoda wysyłająca email
 * @param to adresat
 * @param subject tytuł wiadomośći
 * @param content treść wiadomośći
 * @throws MessagingException wyjątki
 */
	public void send(String to, String subject, String content)
			throws MessagingException {

		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtps.auth", "true");

		// Inicjalizacja sesji
		Session mailSession = Session.getDefaultInstance(props);

		// ustawienie debagowania, jeśli nie chcesz oglądać logów to usuń
		// linijkę poniżej lub zmień wartość na false
		mailSession.setDebug(true);

		// Tworzenie wiadomości email
		MimeMessage message = new MimeMessage(mailSession);
		message.setSubject(subject);
		message.setContent(content, "text/html; charset=ISO-8859-2");
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

		Transport transport = mailSession.getTransport();
		transport.connect(HOST, PORT, FROM, PASSWORD);

		// wysłanie wiadomości
		transport.sendMessage(message,
				message.getRecipients(Message.RecipientType.TO));
		transport.close();
	}
}
