package pl.testy.service;

import java.util.UUID;

import javax.mail.MessagingException;
/**
 * Klasa obsługująca powiadomienia
 * @author wojtek
 *
 */
public class NotificationService {

	private static final String URLACCEPT = "http://127.0.0.1:8080/testy/acceptservlet?token=";
	private static final String URLPASSWORD = "http://127.0.0.1:8080/testy/Remindpasswordservlet?token=";

	/**
	 * Tworzenie powiadomienia akceptacji adresu email
	 * @param email adres email
	 * @param login login uzytkownika
	 * @return wygenerowany token
	 */
	public String createAcceptNotification(String email, String login) {

		MailService mailService = new MailService();

		UUID token = UUID.randomUUID();

		String subject = "Witamy w systemie !!!";
		String content = "Witaj " + login
				+ "<br/>Kliknij w link aby aktywować konto: <a href=\"" + URLACCEPT
				+ token + "\">aktywuj</a>";
		try {
			mailService.send(email, subject, content);
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		return token.toString();
	}
/**
 * Tworzenie powiadomienia o zmianie hasła
 * @param email adres email
 * @param login login użytkownika
 * @return wygenerowany token
 */
	public String createPasswordChangeMail(String email, String login) {

		MailService mailService = new MailService();

		UUID token = UUID.randomUUID();

		String subject = "Prośba o zresetowanie hasła";
		String content = "Witaj wysłana została prośba o zresetowanie hasła jeżeli nie jesteś jej autorem poprostu zignoruj ten email " + login
				+ "<br/>Kliknij w link aby zresetować hasło: <a href=\"" + URLPASSWORD
				+ token + "\">aktywuj</a>";
		try {
			mailService.send(email, subject, content);
		} catch (MessagingException e) {
			e.printStackTrace();
			return token.toString();
		}

		return token.toString();

	}
	/**
	 * Tworzenie powiadomienia z nowym hasłem
	 * @param email adres email
	 * @param haslo nowe hasło
	 */
	public void sendNewPasswordMail(String email, String haslo) {

		MailService mailService = new MailService();

	

		String subject = "Zresetowanie hasła";
		String content = "Witaj  prośba o zresetowanie hasła została zaakceptowana nowe hasło to  " + haslo;
		try {
			mailService.send(email, subject, content);
		} catch (MessagingException e) {
			e.printStackTrace();
			
		}

		

	}
}
