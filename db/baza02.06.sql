-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 02 Cze 2012, 21:39
-- Wersja serwera: 5.5.16
-- Wersja PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `testdb`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `aktywnytest`
--

CREATE TABLE IF NOT EXISTS `aktywnytest` (
  `id_aktywnytest` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_test` int(10) unsigned NOT NULL,
  `id_grupy` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_aktywnytest`),
  KEY `id_test` (`id_test`),
  KEY `id_grupy` (`id_grupy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `ankiety`
--

CREATE TABLE IF NOT EXISTS `ankiety` (
  `id_ankiety` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_autor` int(10) unsigned NOT NULL,
  `nazwa_ankiety` varchar(255) DEFAULT NULL,
  `stan` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_ankiety`),
  KEY `id_autor` (`id_autor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `ankiety`
--

INSERT INTO `ankiety` (`id_ankiety`, `id_autor`, `nazwa_ankiety`, `stan`) VALUES
(1, 1, 'Ankieta1', 'A'),
(2, 1, 'Ankieta2', 'A'),
(3, 1, 'Ankieta o ludziach', 'AR');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `grupy`
--

CREATE TABLE IF NOT EXISTS `grupy` (
  `id_grupy` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_klasy` int(10) unsigned NOT NULL,
  `nazwa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_grupy`),
  KEY `id_klasy` (`id_klasy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Zrzut danych tabeli `grupy`
--

INSERT INTO `grupy` (`id_grupy`, `id_klasy`, `nazwa`) VALUES
(5, 9, 'grupa3'),
(6, 10, 'bleee'),
(7, 11, 'grupa1'),
(8, 12, 'twoja grupa'),
(15, 9, 'grupa1'),
(16, 9, 'grupa2'),
(18, 16, 'nowa grupa'),
(19, 17, 'probna grupa1'),
(21, 19, 'Grupa wszyscy'),
(24, 16, 'xxx'),
(25, 9, 'grupa 4'),
(26, 16, 'xxx1'),
(27, 9, 'grupaall'),
(29, 22, 'grupa1'),
(30, 22, 'grupa2'),
(31, 22, 'grupa4'),
(32, 23, 'grupa1'),
(33, 23, 'sdfsdfsdf'),
(34, 23, 'sdfsdfsdf'),
(37, 26, 'Grupa wszyscy');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `klasy`
--

CREATE TABLE IF NOT EXISTS `klasy` (
  `id_klasy` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_dydaktyka` int(10) unsigned NOT NULL,
  `nazwa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_klasy`),
  KEY `id_dydaktyka` (`id_dydaktyka`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Zrzut danych tabeli `klasy`
--

INSERT INTO `klasy` (`id_klasy`, `id_dydaktyka`, `nazwa`) VALUES
(9, 1, 'nasza klasa4'),
(10, 1, 'wyswietlanie danych'),
(11, 1, 'moja klasa'),
(12, 1, 'twoja klasa i moja'),
(15, 1, 'nasza klasa4'),
(16, 1, 'klasa5c'),
(17, 1, 'Klasa orÅ?Ã³w'),
(19, 29, 'Klasa1'),
(22, 1, 'naszklasa2'),
(23, 1, 'klasa9'),
(24, 1, 'Klasamoja'),
(26, 36, 'Klasa1');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `mail_token`
--

CREATE TABLE IF NOT EXISTS `mail_token` (
  `id_mail_token` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_users` int(10) unsigned NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `data_wysłania` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_mail_token`),
  KEY `id_users` (`id_users`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `mail_token`
--

INSERT INTO `mail_token` (`id_mail_token`, `id_users`, `token`, `data_wysłania`) VALUES
(3, 36, 'fed5e9b9-76e6-4b8d-9019-44da47420e2a', '2012-06-01 12:27:03');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `odpowiedz`
--

CREATE TABLE IF NOT EXISTS `odpowiedz` (
  `id_odpowiedz` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pytanie` int(10) unsigned NOT NULL,
  `tresc_odpowiedzi` longtext,
  `ilosc_punktow` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_odpowiedz`),
  KEY `id_pytanie` (`id_pytanie`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Zrzut danych tabeli `odpowiedz`
--

INSERT INTO `odpowiedz` (`id_odpowiedz`, `id_pytanie`, `tresc_odpowiedzi`, `ilosc_punktow`) VALUES
(1, 1, NULL, 1),
(2, 2, NULL, 1),
(3, 3, NULL, 1),
(4, 19, NULL, 1),
(5, 19, NULL, 1),
(6, 19, NULL, 0),
(7, 20, NULL, 5),
(8, 20, NULL, 6),
(9, 20, NULL, 7),
(10, 22, NULL, 1),
(11, 22, NULL, 0),
(12, 22, NULL, 0),
(13, 23, NULL, 1),
(14, 23, NULL, 0),
(15, 23, NULL, 1),
(16, 24, NULL, 1),
(17, 24, NULL, 5),
(18, 25, 'asdadasdasdasdasd', 1),
(19, 26, 'odpowiedz jakas', 1),
(20, 27, 'sdfsdfsdfsdf', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `odpowiedz_ankiety`
--

CREATE TABLE IF NOT EXISTS `odpowiedz_ankiety` (
  `id_odpowiedz_ankiety` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pytania_ankiety` int(10) unsigned NOT NULL,
  `tresc_odpowiedzi` text,
  `ilosc_klikniec` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_odpowiedz_ankiety`),
  KEY `id_pytania_ankiety` (`id_pytania_ankiety`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `odpowiedz_ankiety`
--

INSERT INTO `odpowiedz_ankiety` (`id_odpowiedz_ankiety`, `id_pytania_ankiety`, `tresc_odpowiedzi`, `ilosc_klikniec`) VALUES
(1, 1, NULL, 0),
(2, 2, NULL, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `pytania`
--

CREATE TABLE IF NOT EXISTS `pytania` (
  `id_pytanie` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_test` int(10) unsigned NOT NULL,
  `nazwa_pytania` text,
  `foto` varchar(1500) DEFAULT NULL,
  `video` varchar(1500) DEFAULT NULL,
  PRIMARY KEY (`id_pytanie`),
  KEY `id_test` (`id_test`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania` (`id_pytanie`, `id_test`, `nazwa_pytania`, `foto`, `video`) VALUES
(1, 5, 'sdfsfsf', NULL, NULL),
(2, 5, 'sdfsdfsfs', NULL, NULL),
(3, 5, 'sdfsdfsf', NULL, NULL),
(9, 8, 'sdfsdfsdf', NULL, NULL),
(10, 9, '', NULL, NULL),
(11, 10, '', NULL, NULL),
(12, 11, '', NULL, NULL),
(13, 12, '', NULL, NULL),
(14, 13, 'sdffff', NULL, NULL),
(15, 14, '', NULL, NULL),
(16, 4, 'aaaaaaaaaaaa', NULL, NULL),
(17, 16, 'aaaaaaaaaaaa', NULL, NULL),
(18, 4, 'pytanie 1', NULL, NULL),
(19, 18, 'dfhhsdfsdfsdffd', NULL, NULL),
(20, 18, 'dfhhsdfsdfsdffdaaa', NULL, NULL),
(21, 19, 'probne pytanie', NULL, NULL),
(22, 20, 'pytanie jakies', NULL, NULL),
(23, 20, 'pytanie jakies', NULL, NULL),
(24, 21, 'Pytanie nr 1 jak nazywa siÃ?Â? ..', NULL, NULL),
(25, 22, '<b>Mojkod</br><hr><hr><hr><hr><hr>', NULL, NULL),
(26, 24, 'pytanie z video', '', 'http://youtu.be/TPKRcosRocI'),
(27, 25, 'co widzisz na filmie', '', '<iframe width="420" height="315" src="http://www.youtube.com/embed/TPKRcosRocI" frameborder="0" allowfullscreen></iframe>');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `pytania_ankiety`
--

CREATE TABLE IF NOT EXISTS `pytania_ankiety` (
  `id_pytania_ankiety` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ankiety` int(10) unsigned DEFAULT NULL,
  `nazwa_pytania` text,
  `foto` varchar(1500) DEFAULT NULL,
  `video` varchar(1500) DEFAULT NULL,
  PRIMARY KEY (`id_pytania_ankiety`),
  KEY `id_ankiety` (`id_ankiety`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Zrzut danych tabeli `pytania_ankiety`
--

INSERT INTO `pytania_ankiety` (`id_pytania_ankiety`, `id_ankiety`, `nazwa_pytania`, `foto`, `video`) VALUES
(1, 2, 'aaaaaaaaaaaa', NULL, NULL),
(2, 3, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id_role` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_testera` int(10) unsigned NOT NULL,
  `id_dydaktyka` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_role`),
  KEY `id_testera` (`id_testera`),
  KEY `id_dydaktyka` (`id_dydaktyka`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Zrzut danych tabeli `role`
--

INSERT INTO `role` (`id_role`, `id_testera`, `id_dydaktyka`) VALUES
(1, 4, 1),
(2, 5, 1),
(4, 7, 1),
(12, 27, 25),
(13, 30, 29),
(14, 31, 29),
(15, 32, 29);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `tagi_ankiety`
--

CREATE TABLE IF NOT EXISTS `tagi_ankiety` (
  `id_tagi_ankiety` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_ankiety` int(10) unsigned NOT NULL,
  `nazwa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tagi_ankiety`),
  KEY `id_ankiety` (`id_ankiety`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `tagi_test`
--

CREATE TABLE IF NOT EXISTS `tagi_test` (
  `id_tagi_test` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_test` int(10) unsigned NOT NULL,
  `nazwa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tagi_test`),
  KEY `id_test` (`id_test`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id_test` int(10) unsigned NOT NULL,
  `id_dydaktyka` int(10) unsigned NOT NULL,
  `nazwa_testu` varchar(255) CHARACTER SET utf8 COLLATE utf8_polish_ci DEFAULT NULL,
  `opis_testu` varchar(255) DEFAULT NULL,
  `stan` varchar(255) DEFAULT NULL,
  `czas_trwania` int(10) unsigned DEFAULT NULL,
  `max_punkty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_test`),
  KEY `id_dydaktyka` (`id_dydaktyka`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `test`
--

INSERT INTO `test` (`id_test`, `id_dydaktyka`, `nazwa_testu`, `opis_testu`, `stan`, `czas_trwania`, `max_punkty`) VALUES
(1, 1, 'adasd', 'opis', 'A', 0, 0),
(2, 1, 'przyk?adowy est?', '', 'N', 10, 100),
(3, 1, 'testyyy222222222', 'testyyyyyyyy2', 'N', NULL, NULL),
(4, 1, 'testyyy222222222', 'testyyyyyyyy3', 'A', NULL, NULL),
(5, 1, 'nazwatestu', 'opis', 'A', 0, 0),
(8, 1, 'sdfsdf', '', 'N', NULL, NULL),
(9, 1, 'test', '', 'N', NULL, NULL),
(10, 1, 'test', '', 'N', NULL, NULL),
(11, 1, 'aaaaaaaa', '', 'N', NULL, NULL),
(12, 1, 'aaaaaaaa', '', 'N', NULL, NULL),
(13, 1, 'aaaaaaaa', '', 'N', NULL, NULL),
(14, 1, 'aaaaaaaa', '', 'N', NULL, NULL),
(15, 1, 'test', 'opis testu', 'AR', 10, 100),
(16, 1, 'Test jakis', 'opis testu', 'N', 10, 10),
(17, 1, 'teraz test', 'moj opis', 'N', NULL, NULL),
(18, 1, 'Test1', 'dfhhsdfsdfsdffd', 'A', NULL, NULL),
(19, 1, 'probny test', '', 'N', NULL, NULL),
(20, 1, 'Test o zachowaniu', 'Test o zachowaniu Test o zachowaniu Test o zachowaniu', 'A', NULL, NULL),
(21, 1, 'PrzykÃ?Â?adowy test', 'PrzykÃ?Â?adowy opis', 'N', 10, 20),
(22, 1, 'testHTML', 'opis', 'A', 100, 10),
(23, 1, 'Testvideo', 'opis', 'N', 40, 10),
(24, 1, 'Testvideo', 'opis', 'A', 40, 10),
(25, 1, 'probny testvideo2', '', 'A', 100, 10);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `tester_grupa`
--

CREATE TABLE IF NOT EXISTS `tester_grupa` (
  `id_tester_grupa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_testera` int(10) unsigned NOT NULL,
  `id_grupy` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_tester_grupa`),
  KEY `id_testera` (`id_testera`),
  KEY `id_grupy` (`id_grupy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Zrzut danych tabeli `tester_grupa`
--

INSERT INTO `tester_grupa` (`id_tester_grupa`, `id_testera`, `id_grupy`) VALUES
(2, 31, 21),
(3, 30, 21),
(4, 32, 21),
(5, 7, 21);

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_users` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imie` varchar(255) DEFAULT NULL,
  `nazwisko` varchar(255) DEFAULT NULL,
  `haslo` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `data_rejestracji` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `role` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `stan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_users`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id_users`, `imie`, `nazwisko`, `haslo`, `email`, `data_rejestracji`, `role`, `login`, `stan`) VALUES
(1, 'tontek', 'koszycki', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.pl', NULL, 'admin', 'admin', 'N'),
(3, 'user', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user@user.pl', '2012-05-17 14:05:19', 'user', 'user', 'N'),
(4, 'test', 'test', '5a105e8b9d40e1329780d62ea2265d8a', 'sdfsdf@o2.pl', '2012-05-20 14:57:46', 'user', 'test1', 'N'),
(5, 'test', 'test', 'ad0234829205b9033196ba818f7a872b', 'sdfsdf@o2.pl', '2012-05-20 15:01:22', 'user', 'test2', 'N'),
(6, 'uczen', 'uczen', 'd0bc4421f0910959b65e9c80aa4746c1', 'uczen@uczen.pl', '2012-05-26 10:44:25', 'user', 'uczen1', 'N'),
(7, 'wojtek', 'bolek', '486d9ff14291c7e88d9e69147c1a351f', 'bolek@bolek.pl', '2012-05-26 11:05:29', 'user', 'bolek', 'N'),
(25, 'Leszek', 'Kotulski', '207023ccb44feb4d7dadca005ce29a64', 'kotulski@agh.edu.pl', '2012-05-28 11:07:27', 'admin', 'lekotulski', 'N'),
(26, 'Wojciech', 'Koszycki', '207023ccb44feb4d7dadca005ce29a64', 'email@email.pl', '2012-05-28 11:13:26', 'admin', 'wokoszycki', 'N'),
(27, 'Wojtas', 'Koszycki', 'e97deb614555a47601c8a5941f12a277', 'woj@em.pl', '2012-05-28 11:38:53', 'user', 'mojloginek', 'N'),
(29, 'Zbigniew', 'Dendzik', '1527c9aa9e4e80bb7f8837d72f9e4494', 'dendzik@dendzik.pl', '2012-05-28 16:48:20', 'admin', 'dendzik', 'N'),
(30, 'uzytkownik', 'kkkkkkk', 'd8e71dbd1afc289a4b102eeadeb6f363', 'assada@o2.pl', '2012-05-28 16:51:28', 'user', 'kkkkkkk', 'N'),
(31, 'Ania', 'WesoÅ?a', '5f4dcc3b5aa765d61d8327deb882cf99', 'anka87@o2.pl', '2012-05-28 18:53:59', 'user', 'anka', 'N'),
(32, 'Piotr', 'Guzek', '5f4dcc3b5aa765d61d8327deb882cf99', 'guzek@o2.pl', '2012-05-28 19:23:37', 'user', 'guzek', 'N'),
(36, 'Wojtek', 'Wojtek', '8b8609c381cdbc6d6ff61c383b0adab0', 'koszycki.wojciech@gmail.com', '2012-06-01 12:27:00', 'admin', 'Wojtek', 'A');

-- --------------------------------------------------------

--
-- Struktura tabeli dla  `wyniki`
--

CREATE TABLE IF NOT EXISTS `wyniki` (
  `id_wyniki` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_test` int(10) unsigned NOT NULL,
  `id_testera` int(10) unsigned NOT NULL,
  `zdobyty_wynik` int(10) unsigned NOT NULL,
  `data_start` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `data_stop` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_wyniki`),
  KEY `id_test` (`id_test`),
  KEY `id_testera` (`id_testera`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Zrzut danych tabeli `wyniki`
--

INSERT INTO `wyniki` (`id_wyniki`, `id_test`, `id_testera`, `zdobyty_wynik`, `data_start`, `data_stop`) VALUES
(1, 5, 7, 0, '2012-05-27 18:05:42', NULL),
(2, 4, 7, 0, '2012-05-31 16:58:14', NULL),
(3, 18, 7, 0, '2012-05-31 17:03:45', NULL),
(4, 20, 7, 2, '2012-06-02 12:06:58', '2012-06-02 12:07:15'),
(5, 22, 7, 1, '2012-06-02 18:35:21', '2012-06-02 18:36:27'),
(6, 24, 7, 0, '2012-06-02 19:18:16', NULL),
(7, 24, 7, 0, '2012-06-02 19:27:25', NULL),
(8, 25, 7, 2, '2012-06-02 19:35:48', '2012-06-02 19:36:21');

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `aktywnytest`
--
ALTER TABLE `aktywnytest`
  ADD CONSTRAINT `aktywnytest_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `test` (`id_test`) ON DELETE CASCADE,
  ADD CONSTRAINT `aktywnytest_ibfk_2` FOREIGN KEY (`id_grupy`) REFERENCES `grupy` (`id_grupy`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `ankiety`
--
ALTER TABLE `ankiety`
  ADD CONSTRAINT `ankiety_ibfk_1` FOREIGN KEY (`id_autor`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `grupy`
--
ALTER TABLE `grupy`
  ADD CONSTRAINT `grupy_ibfk_1` FOREIGN KEY (`id_klasy`) REFERENCES `klasy` (`id_klasy`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `klasy`
--
ALTER TABLE `klasy`
  ADD CONSTRAINT `klasy_ibfk_1` FOREIGN KEY (`id_dydaktyka`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `mail_token`
--
ALTER TABLE `mail_token`
  ADD CONSTRAINT `mail_token_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `odpowiedz`
--
ALTER TABLE `odpowiedz`
  ADD CONSTRAINT `odpowiedz_ibfk_1` FOREIGN KEY (`id_pytanie`) REFERENCES `pytania` (`id_pytanie`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `odpowiedz_ankiety`
--
ALTER TABLE `odpowiedz_ankiety`
  ADD CONSTRAINT `odpowiedz_ankiety_ibfk_1` FOREIGN KEY (`id_pytania_ankiety`) REFERENCES `pytania_ankiety` (`id_pytania_ankiety`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `pytania`
--
ALTER TABLE `pytania`
  ADD CONSTRAINT `pytania_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `test` (`id_test`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `pytania_ankiety`
--
ALTER TABLE `pytania_ankiety`
  ADD CONSTRAINT `pytania_ankiety_ibfk_1` FOREIGN KEY (`id_ankiety`) REFERENCES `ankiety` (`id_ankiety`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `role`
--
ALTER TABLE `role`
  ADD CONSTRAINT `role_ibfk_1` FOREIGN KEY (`id_testera`) REFERENCES `users` (`id_users`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_ibfk_2` FOREIGN KEY (`id_dydaktyka`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `tagi_ankiety`
--
ALTER TABLE `tagi_ankiety`
  ADD CONSTRAINT `tagi_ankiety_ibfk_1` FOREIGN KEY (`id_ankiety`) REFERENCES `ankiety` (`id_ankiety`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `tagi_test`
--
ALTER TABLE `tagi_test`
  ADD CONSTRAINT `tagi_test_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `test` (`id_test`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `test_ibfk_1` FOREIGN KEY (`id_dydaktyka`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `tester_grupa`
--
ALTER TABLE `tester_grupa`
  ADD CONSTRAINT `tester_grupa_ibfk_1` FOREIGN KEY (`id_testera`) REFERENCES `users` (`id_users`) ON DELETE CASCADE,
  ADD CONSTRAINT `tester_grupa_ibfk_2` FOREIGN KEY (`id_grupy`) REFERENCES `grupy` (`id_grupy`) ON DELETE CASCADE;

--
-- Ograniczenia dla tabeli `wyniki`
--
ALTER TABLE `wyniki`
  ADD CONSTRAINT `wyniki_ibfk_1` FOREIGN KEY (`id_test`) REFERENCES `test` (`id_test`) ON DELETE CASCADE,
  ADD CONSTRAINT `wyniki_ibfk_2` FOREIGN KEY (`id_testera`) REFERENCES `users` (`id_users`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
