<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="top.jsp" %>
  </div>
   <%@include file="menu.jsp" %>
     
    <div class="content">
      <div class="content_top">
      <h2><img src="../images/script_32x32.png" alt="script" >Regulamin serwisu</h2>
      </div>
      <div class="content_diff">
        <h4>Spis pojęć</h4>
        

<ul>
<li>Administrator - podmiot po stronie właściciela Serwisu, który zarządza serwisem.

<li>Użytkownik - osoba, która odwiedza stronę i/lub posiada zarejestrowane konto.

<li>Test - zbiór pytań, który pozwala uzyskać informację zawierającą prawidłowe odpowiedzi i wynik testu wyrażony w procentach.

<li>Usługa - usługa polegająca na udostępnianiu Użytkownikom Serwisu materiałów edukacyjnych lub wyników testów opublikowanych w Serwisie i rozwiązanych przez Użytkownika.

<li>Właściciel serwisu - Wojciech Koszycki Pewel Mała ul.Leszczynowa 7 34-331 Świnna.

<li>Serwis - jest to strona internetowa dostępna pod adresem www.testyiankiety.xxxx.pl  wraz z treściami się na niej znajdującymi i przeznaczona do użytku publicznego zgodnie z niniejszym regulaminem.


</ul>


<h4>Zasady korzystania z serwisu</h4>
<p>
<ol>
<li>Serwis przeznaczony jest dla Użytkowników korzystających z aplikacji do tworzenia testów

<li> Serwis przeznaczony jest dla Użytkowników korzystających z testów i innych materiałów edukacyjnych zamieszczonych w serwisie.

<li> Korzystanie z Aplikacji do tworzenia testu i/lubankiety przez Użytkownika jest bezpłatne.

<li> Korzystanie z Testu przez Użytkownika jest bezpłatne, jednak uzyskanie wyniku określonego testu następuje po dokonaniu płatności SMS według cennika podanego po zakończeniu testu. Dla testów psychologicznych i testów zawodowych płatność wynosi 2 zł netto (2.44 brutto), dla testów wiedzy 1zł netto (1.22 brutto). Inne materiały edukacyjne (wypracowania, sprawdziany) mogą miec różne ceny.

<li> Użytkownikiem serwisu może zostać osoba pełnoletnia lub osoba niepełnoletnia, która uzyskała zgodę swoich przedstawicieli ustawowych lub opiekunów.

<li> Użytkownik Serwisu nie ma praw do roszczenia z tytułu jakichkolwiek błędów wynikających z winy aplikacji

<li> Użytkownik ponosi wyłączną i osobistą odpowiedzialność za bezprawny oraz niezgodny z niniejszym regulaminem sposób korzystania z Serwisu, z uwzględnieniem odpowiedzialności karnej oraz cywilnej.

<li> Właściciel serwisu zastrzega sobie prawo do wprowadzania zmian w niniejszym regulaminie bez konieczności informowania Użytkownika.

<li> Użytkownik jest odpowiedzialny za przestrzeganie warunków korzystania z niniejszego regulaminu.

<li> Pomoc dostępna jest pod adresem pomoc@testyiankiety.pl

<li> Regulamin obowiązuje od 31 maja 2012.

<li> Kontakt z Administratorem za następuje pomocą formularza kontaktu dostępnego na stronie www.testyiankiety.xxxx.pl/files/kontakt 
</ol>

<h4>Korzystanie z Usługi</h4>
<p>
<ol>
<li> Użytkownikiem Usługi może być każda pełnoletnia osoba fizyczna. Osoby niepełnoletnie, by zostać Użytkownikiem Usługi, muszą posiadać zgodę swoich przedstawicieli ustawowych lub opiekunów.
<li> Użytkownik, który chce skorzystać z Usługi, wysyła wiadomość SMS na numer podany po rozwiązaniu wybranego testu oraz o treści określonej w opublikowanych materiałach reklamowych dotyczących Usługi.
<li> Po prawidłowym wysłaniu wiadomości SMS Użytkownik otrzymuje na numer telefonu, z którego pochodził SMS Premium, wiadomość zwrotną, która zawiera specjalny Kod Dostępu do wpisania w Serwisie.
<li> Do korzystania z Usługi nie mają zastosowania ulgi dla grup abonentów na opłaty abonamentowe oraz usługi dodatkowe.
<li> Dostawca Usługi i Administrator nie ponoszą odpowiedzialności za:
• treść zamieszczanych testów i/lub ankiet przez Użytkownika w ramach korzystania z Usługi,<br>
• korzystanie z Usługi przez osoby nieuprawnione do użytkowania serwisu<br>
• szkody spowodowane nieprawidłowym korzystaniem przez Użytkownika z Usługi,<br>
• problemy w funkcjonowaniu Usługi, jeżeli nastąpiły one wskutek zdarzeń niezależnych od Dostawcy Usługi lub Administratora, w szczególności wskutek zdarzeń losowych o charakterze siły wyższej,<br>
• korzystanie z Usługi przez Użytkowników niezgodnie z postanowieniami niniejszego Regulaminu lub bez zapoznania się z Regulaminem, w tym zwłaszcza problemy wynikające z niewłaściwego zrozumienia przez Użytkownika charakteru Usługi,<br>
• przerwy w świadczeniu Usługi zaistniałe z przyczyn technicznych (np. konserwacja, przegląd, wymiana sprzętu) lub niezależnych od Dostawcy Usługi lub Administratora.<br>
</ol>
<h4>Reklamacje</h4>
<p>
<ol>
<li> Wszelkie reklamacje dotyczące niewłaściwego działania Usługi należy kierować do Administratora nie później niż w ciągu jednego dnia roboczego licząc od dnia wystąpienia nieprawidłowości w korzystaniu z Usługi. Treść reklamacji powinna wskazywać nieprawidłowości w realizacji Usługi oraz inne informacje ułatwiające sprawne rozpatrzenie zgłoszenia.
<li> Reklamacje można kierować:
<li> drogą mailową na adres pomoc@testyiankiety.pl
<li> Reklamacje będą rozpatrywane w terminie do 14 dni od daty otrzymania reklamacji przez Administratora. W szczególnych przypadkach Administrator zastrzega sobie możliwość wydłużenia terminu rozpatrzenia reklamacji do 30 dni po uprzednim poinformowaniu Użytkownika o takiej konieczności.
<li> W przypadku uznania reklamacji Użytkownika, ma on prawo do otrzymania dostępu do Usługi którą zamówił, Użytkownik nie jest jednak uprawniony do wystąpienia o odszkodowanie lub zwrot należności.
</ol>


 			</div>

      <div class="content_bottom"></div>
    </div>

   <%@include file="foot.jsp" %>
</div>
</body>
</html>

