<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div id="main">
	<div class="sidebar">
		<div class="sidebar_top"></div>
		<h3>Menu</h3>


		<ul class="menujquery" id="mjquery">
			<li class="item1"><a href="#">Psychotesty <span></span></a>
				<ul>
					<li class="subitem1"><a href="<%=request.getContextPath()%>/menu/naosobowosc.jsp">Na Osobowość<span></span></a></li>
					<li class="subitem2"><a href="<%=request.getContextPath()%>/menu/nainteligencje.jsp">Na inteligencje <span></span></a></li>
					<li class="subitem3"><a href="<%=request.getContextPath()%>/menu/psychotestyinne.jsp">Inne <span></span></a></li>
				</ul></li>
			<li class="item2"><a href="#">Testy instytucji <span></span></a>
				<ul>
					<li class="subitem1"><a href="<%=request.getContextPath()%>/menu/testypolicja.jsp">Policja <span></span></a></li>
					<li class="subitem2"><a href="<%=request.getContextPath()%>/menu/testystraz.jsp">Straż pożarna <span></span></a></li>
					<li class="subitem3"><a href="<%=request.getContextPath()%>/menu/testyinne.jsp">Inne <span></span></a></li>
				</ul></li>
		</ul>



		<!--initiate accordion-->
		<script type="text/javascript">
			$(function() {

				var menujquery_ul = $('.menujquery > li > ul'), menujquery_a = $('.menujquery > li > a');

				menujquery_ul.hide();

				menujquery_a.click(function(e) {
					e.preventDefault();
					if (!$(this).hasClass('active')) {
						menujquery_a.removeClass('active');
						menujquery_ul.filter(':visible').slideUp('normal');
						$(this).addClass('active').next().stop(true, true)
								.slideDown('normal');
					} else {
						$(this).removeClass('active');
						$(this).next().stop(true, true).slideUp('normal');
					}
				});

			});
		</script>
		<%
			if (request.isUserInRole("admin")) {
		%>
		<h3>Panel</h3>
		<ul class="menujquery">
			<li class="item1"><a href="#">Stwórz nowy <span></span></a>
				<ul>
					<li class="subitem1"><a
						href="<%=request.getContextPath()%>/admin/makeSurvey.jsp">Stwórz
							Ankietę <span></span>
					</a></li>
					<li class="subitem2"><a
						href="<%=request.getContextPath()%>/admin/makeTest.jsp">Stwórz
							Test <span></span>
					</a></li>
				</ul></li>
			<li class="item2"><a href="#">Zarządzaj stworzonymi <span></span></a>
				<ul>
					<li class="subitem1"><a
						href="<%=request.getContextPath()%>/admin/showSurveys.jsp">Przegląd
							Ankiet <span></span>
					</a></li>
					<li class="subitem2"><a
						href="<%=request.getContextPath()%>/admin/showTests.jsp">Przegląd
							Testów <span></span>
					</a></li>
					<li class="subitem3"><a
						href="<%=request.getContextPath()%>/admin/archive.jsp">Archiwum
							<span></span>
					</a></li>
				</ul></li>
			<li class="item3"><a href="#">Twoi Użytkownicy<span></span></a>
				<ul>
					<li class="subitem1"><a
						href="<%=request.getContextPath()%>/admin/addTester.jsp">Dodaj
							Testera <span></span>
					</a></li>
					<li class="subitem2"><a
						href="<%=request.getContextPath()%>/admin/showUsers.jsp">Przegląd
							Użytkowników <span></span>
					</a></li>
				</ul></li>
			<li class="item4"><a href="#">Twoje Klasy <span></span></a>
				<ul>
					<li class="subitem1"><a
						href="<%=request.getContextPath()%>/admin/showClasses.jsp">Przegląd
							Klas <span></span>
					</a></li>
					<li class="subitem2"><a
						href="<%=request.getContextPath()%>/admin/addClass.jsp">Dodaj
							Klasę <span></span>
					</a></li>
				</ul></li>
			<li class="item5"><a href="#">Inne <span></span></a>
				<ul>
					<li class="subitem3"><a href="<%=request.getContextPath()%>/admin/sendNotification.jsp">Wyślij Powiadomienia <span></span></a></li>
					<li class="subitem4"><a href="<%=request.getContextPath()%>/admin/uploadform.jsp">Udostępnij Materiały <span></span></a></li>
					<li class="subitem4"><a href="<%=request.getContextPath()%>/admin/fileList.jsp">Twoje Materiały <span></span></a></li>
				</ul></li>
			<li class="item6"><a href="#">Twój Profil <span></span></a>
				<ul>
					<li class="subitem1"><a
						href="<%=request.getContextPath()%>/admin/editProfile.jsp">Edytuj
							Profil <span></span>
					</a></li>

				</ul></li>
		</ul>
		<%
			}
		%>
		<%
			if (request.isUserInRole("user")) {
		%>
		<h3>Panel</h3>
		<ul class="menujquery">
			<li class="item1"><a href="#">Podejmij Wyzwanie <span></span></a>
				<ul>
					<li class="subitem1"><a
						href="<%=request.getContextPath()%>/user/showTests.jsp">Aktualne
							Testy <span></span>
					</a></li>
					<li class="subitem2"><a
						href="<%=request.getContextPath()%>/user/showSurvey.jsp">Aktualne
							Ankiety <span></span>
					</a></li>
				</ul></li>
			<li class="item2"><a href="#">Materiały Dydaktyczne <span></span></a>
				<ul>
					<li class="subitem1"><a
						href="<%=request.getContextPath()%>/user/showMaterials.jsp">Pobierz
							materiały <span></span>
					</a>
				</ul></li>
			<li class="item3"><a href="#">Wyniki<span></span></a>
				<ul>
					<li class="subitem1"><a
						href="<%=request.getContextPath()%>/user/showResult.jsp">Rozwiązane
							testy <span></span>
					</a></li>
					<li class="subitem2"><a
						href="<%=request.getContextPath()%>/user/archive.jsp">Archiwum
							<span></span>
					</a></li>
				</ul></li>

			<li class="item4"><a href="#">Profil<span></span></a>
				<ul>
					<li class="subitem1"><a
						href="<%=request.getContextPath()%>/user/editProfile.jsp">Wyświetl
							profil <span></span>
					</a>
				</ul></li>
		</ul>
		<%
			}
		%>


		<div class="form"></div>
		<div class="kontakt">
			<img src="<%=request.getContextPath()%>/images/user_24x24.png" alt="user" ><br>

			<span> <%
 	if (request.getUserPrincipal() == null) {
 %> Status logowania:<br> Niezalogowany<br> <b>ZALOGUJ DO
					MOJE TESTY</b><br> <a
				href="<%=request.getContextPath()%>/admin/index.jsp">Dydaktyk</a><br>
				<a href="<%=request.getContextPath()%>/user/index.jsp">Tester</a> <%
 	} else {
 %> Witaj,jestes zalogowany jako: &nbsp; <%=request.getUserPrincipal().getName()%>
				<br>
				<a href="<%=request.getContextPath()%>/login/logout.jsp"><b>WYLOGUJ</b></a>
				<%
					}
				%>
			</span>
		</div>
		<div class="form"></div>

		<img alt="" src="<%=request.getContextPath()%>/images/reklamy.png" >

		<div class="sidebar_bottom"></div>
	</div>