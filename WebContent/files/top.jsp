<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div id="logo">
    <h1> </h1>
  </div>
  <div id="menu">
    <div id="menu_begin"></div>
    <ul>
      <li><img src="<%=request.getContextPath()%>/images/house_16x16.png" alt="house" ><a href="<%=request.getContextPath()%>/index.jsp"> Strona Główna</a></li>
      <li><img src="<%=request.getContextPath()%>/images/key_2_16x16.png" alt="key" ><a href="<%=request.getContextPath()%>/files/rejestracja.jsp"> Rejestracja</a></li>
      <li><img src="<%=request.getContextPath()%>/images/zoom_in_16x16.png" alt="zoom" ><a href="<%=request.getContextPath()%>/files/jakToDziala.jsp"> Jak to działa ?</a></li>
      <li><img src="<%=request.getContextPath()%>/images/script_16x16.png" alt="script" ><a href="<%=request.getContextPath()%>/files/regulamin.jsp"> Regulamin</a></li>
	  <li><img src="<%=request.getContextPath()%>/images/mail_16x16.png" alt="mail" ><a href="<%=request.getContextPath()%>/files/kontakt.jsp"> Kontakt</a></li>
    </ul>
    <div id="menu_end"></div>