<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >
<script type="text/javascript" src="../jscript/loadjsContactForm.js"></script>
<script type="text/javascript" src="../jscript/validateContactForm.js"></script>
</head>
<body>
<div class="external">
  <%@include file="top.jsp" %>
  </div>
   <%@include file="menu.jsp" %>
     
    <div class="content">
      <div class="content_top">
      <h2><img src="../images/mail_32x32.png" alt="email" > Kontakt</h2>
      </div>
      <div class="content_diff">
        <h4>Formularz kontaktowy</h4>
        

     
<form action="../Sendmailservlet"  method="post" id="kontaktform">
   <table>   
   <tr>
        <td><label for="title">Temat:</label></td>
       <td> <input id="title" name="title" type="text">        
        <div id="err_title" class="error"> </div></td>    
   </tr>
   <tr> 
       	<td><label>Imię i nazwisko:</label></td>
        <td><input id="authorname" name="authorname" type="text" >   
        <div id="err_authorname" class="error"> </div></td>        
   </tr>
   <tr>
        <td><label>E-mail:</label></td>
        <td><input id="email" name="email" type="text" >    
        <div id="err_email" class="error"> </div></td>       
   </tr>
   <tr>
        <td><label>Treść wiadomości:</label></td>
        <td><textarea id="text" name="text" cols="40" rows="10"></textarea>
        <div id="err_text" class="error"> </div></td>   
    </tr>
</table>
  <div class="captcha"> <script type="text/javascript"
      src="http://api.recaptcha.net/challenge?k=6LfnstASAAAAAKdprsqhCrPMzrfWJDtFqf04fOi_">
   </script>
   <noscript>
       <iframe src="http://api.recaptcha.net/noscript?k=6LfnstASAAAAAKdprsqhCrPMzrfWJDtFqf04fOi_"
           height="300" width="500" frameborder="0"></iframe>
       <textarea name="recaptcha_challenge_field" rows="3" cols="40">
       </textarea>
       <input type="hidden" name="recaptcha_response_field" 
           value="manual_challenge" >
   </noscript>
   </div>
       <div class="obok"> <input class="przycisk" id="sendbutton" name="" type="submit"  value="Wyślij"></div>
</form>
 			</div>

      <div class="content_bottom"></div>
    </div>

   <%@include file="foot.jsp" %>
</div>
</body>
</html>

