/*
Licencja na u�ytek prywatny i komercyjny. Wymaga pozostawiania poni�szych danych o autorze i pochodzeniu skryptu.
Autor: Labsta.com Laboratorium Designu
Skrypt pochodzi ze strony http://websta.pl - Blog o grafice i projektowaniu stron
*/

(function($) {
	
	$.fn.ukryjpola = function(options) {
		
		var defaults = {
			szybkoscpokaz : 'slow',
			szybkoscukryj : 'fast'
		},
		
		settings = $.extend({}, defaults, options); 
		
		$('#'+settings.docelowy+'').hide();
		
		function aktywatorCheckbox() {
			if ($(this).is(':checked')) {
				$('#'+settings.docelowy+'').slideDown(settings.szybkoscpokaz);
			} else {
				$('#'+settings.docelowy+'').slideUp(settings.szybkoscukryj);
			}
		}
		
		function aktywatorRadio() {
			if ($('#'+aktyw+'').is(':checked')) {
				$('#'+settings.docelowy+'').slideDown(settings.szybkoscpokaz);
			} else {
				$('#'+settings.docelowy+'').slideUp(settings.szybkoscukryj);
			}
		}
		
		if ($(this).is("input[type='checkbox']")) {
			$(this).click(aktywatorCheckbox);
		}else if ($(this).is("input[type='radio']")){
			var grupa = $(this).attr("name");
			var aktyw = $(this).attr("id") 
			$("input[name='"+grupa+"']").click(aktywatorRadio);
		}				
		
	} 
	
})(jQuery);