<%@page import="pl.testy.model.Grupa"%>
<%@page import="pl.testy.model.ClassFull"%>
<%@page import="pl.testy.converter.StringConverter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="../images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
	<input type="hidden" name="nazwaklasy" value="">
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>

	<!-- rzutowanie obiektu -->

	<div class="content">
		<div class="content_top">
			<h2>Dodaj Grupę</h2>
		</div>
		<div class="content_diff">
		
			Dodajesz grupę do klasy <b>"<%=request.getAttribute("nazwaklasy")%>"</b>
			<p>Podaj jej nazwę:</p>


			<form method="post" name="myform" action="editgroupservlet">
				<div class="test_description">
					<table width="80px" cellspacing="2" cellpadding="0">
						<tr>
							<td>Nazwa grupy</td>
							<td><input type="text" name="nazwagrupy" value=""></td>
							<input type="hidden" name="nazwaklasy" value="<%=request.getAttribute("nazwaklasy")%>">
						</tr>
					</table>
					<input type="hidden" name="idklasy"
						value="<%=request.getAttribute("idklasy")%>">
				</div>
				<input type="hidden" name="id" value="0">
				<div class="obok"></div>

				<hr>
				<div class="obok">
					<input class="przycisk" type="submit" name="submit"
						value="zapisz grupe">
				</div>
			</form>
		</div>
	</div>
	<%@include file="../files/foot.jsp"%>
</body>
</html>

