<%@page import="pl.testy.converter.StringConverter"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>


	<div class="content">
		<div class="content_top" >
		<h2>
			 Nowa Klasa
		</h2>
		</div>
		<div class="content_diff">

			<menu>
			<li>Podaj jej nazwę, określ grupy. </li>
			<li>Każda klasa wymaga minimum jednej grupy np."wszyscy"</li>
			<li>Nie możesz posiadać dwóch klas o identycznej nazwie.</li>
			<li>Nie możesz posiadać dwóch grup o identycznej nazwie należących do jednej klasy.</li>
			<li>Pamiętaj że zawsze możesz edytować wcześniej stworzone listy</li>
			</menu>
			<form method="post" name="myform" action="addclassservlet">

				<!-- String Converter text->null " " -->
				<%
					StringConverter sc = new StringConverter();

					Integer iloscgrup = 1;
					if (request.getAttribute("iloscgrup") != null) {
						iloscgrup = (Integer) request.getAttribute("iloscgrup");
					}
				%>
				<input type="hidden" value="<%=iloscgrup%>" name="ilosc">


				<div class="test_description">
					<table>
						<tr>
							<td>Nazwa Klasy:</td>
							<td><input type="text" name="f_name"
								value="<%=sc.convertText(request.getParameter("f_name"))%>"
								maxlength="125" size="60"></td>
						</tr>

						
					</table>
				</div>
				<%
					for (int i = 0; i < iloscgrup; i++) {
				%>
				<div class="pytanie">
					<input type="hidden" name="f_privacy" value="0"> <a
						name="pyt1"></a>
					<h2>
						Grupa
						<%=i + 1%></h2>
						<table>
						<tr>
						<td>Nazwa Grupy:</td>
						<td> <input type="text"
						name="f_group[<%=i%>]"
						value="<%=sc.convertText(request.getParameter("f_group[" + i
						+ "]"))%>"
						maxlength="125" size="60"><input type="hidden"
						name="f_qid[1]" value=""></td> 
						</tr>
						</table>
					
					<hr>
					<input type="hidden" name="del_qid" value=""> <input
						type="hidden" name="add_qid" value=""> <input
						type="hidden" name="del_answer_id" value=""> <input
						type="hidden" name="action" value="save"> <input
						type="hidden" name="test_id" value=""> <input
						type="hidden" name="f_created_at" value=""> <input
						type="hidden" name="f_status" value="0">
				</div>
				<%
					}
				%>
				<div class="obok">
				<input class="przycisk" type="submit" name="submit" value="dodaj grupe"> 
				 </div>
				 <input class="przycisk" type="submit" name="submit" value="usun">
				 <hr>
				<div class="obok"> <input class="przycisk" type="submit" name="submit"
					value="zapisz zmiany"></div> 
			</form>
		</div>

		
	</div>
	<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>

