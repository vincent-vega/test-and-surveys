<%@page import="java.util.List"%>
<%@page import="pl.testy.db.manager.ClassDbManager"%>
<%@page import="pl.testy.model.ClassHeader"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="../files/top.jsp" %>
  </div>
   <%@include file="../files/menu.jsp" %>

     
    <div class="content">
      <div class="content_top">
      <h2>Przegląd Klas</h2>
      </div>
      <div class="content_diff">
        <h4>Lista klas</h4>
<% ClassDbManager manager = new ClassDbManager(); 
String logindydaktyka = request.getUserPrincipal().getName();
List<ClassHeader> classlist = manager.classlist(logindydaktyka);
%>

<table>
<tr>
					<td class="tableheader">Nazwa Klasy</td>
					<td class="tableheader">Edytuj</td></tr>
<% for (ClassHeader ch: classlist) { %>
<tr><td><%= ch.getNazwaklasy() %></td><td><a href="../admin/editclassservlet?id=<%= ch.getIdklasy() %>">Edytuj</a></td></tr>
<% } %>
</table>
 			</div>

      <div class="content_bottom"></div>
    </div>

   <%@include file="../files/foot.jsp" %>
</div>
</body>
</html>

