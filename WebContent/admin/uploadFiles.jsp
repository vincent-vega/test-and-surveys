<%@page import="pl.testy.db.manager.UserDbManager"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.FileOutputStream"%>

<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="java.util.*"%>
<%@page import="java.io.File"%>
<%@page import="java.lang.Exception"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="../images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css">

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>


	<div class="content">
		<div class="content_top">
			<h2>Informacje przesłane na serwer</h2>
		</div>
		<div class="content_diff">
			<h4>Dodawanie Pliku</h4>

			<%
				if (ServletFileUpload.isMultipartContent(request)) {
					ServletFileUpload servletFileUpload = new ServletFileUpload(
							new DiskFileItemFactory());
					List fileItemsList = servletFileUpload.parseRequest(request);

					String optionalFileName = "";
					FileItem fileItem = null;

					Iterator it = fileItemsList.iterator();
					while (it.hasNext()) {
						FileItem fileItemTemp = (FileItem) it.next();
						if (fileItemTemp.isFormField()) {
			%>

			<b>Name-value Pair Info:</b><br /> Field name:
			<%=fileItemTemp.getFieldName()%><br /> Field value:
			<%=fileItemTemp.getString()%><br />
			<br />

			<%
				if (fileItemTemp.getFieldName().equals("filename"))
								optionalFileName = fileItemTemp.getString();
						} else
							fileItem = fileItemTemp;
					}

					if (fileItem != null) {
						String fileName = fileItem.getName();
			%>

			<p>
				Typ Zawartości:
				<%=fileItem.getContentType()%><br> Nazwa Pliku:
				<%=fileName%><br> Rozmiar pliku:
				<%=fileItem.getSize()%>bytes<br>
				<br>
			</p>
			<%
			if
			
				(fileItem.getSize() < 5242880){
				String login = request.getUserPrincipal().getName();
				String nazwa = fileItem.getName();
				String typ = fileItem.getContentType();
				UserDbManager udm = new UserDbManager();
				udm.saveFile(fileItem.getInputStream(), login, nazwa,typ);	
				
				%> 
				<b>Plik został dodany</b>
				<% 
			} else {
				%>
				<b>Plik nie może przekraczać 5MB</b>
				
				<% 
			}

			%>

			<%

					}
				}
			%>
		</div>

		<div class="content_bottom"></div>
	</div>

	<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>