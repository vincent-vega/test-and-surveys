<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="pl.testy.model.Plik"%>
<%@page import="java.util.List"%>
<%@page import="pl.testy.db.manager.UserDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="../images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css">

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>

<% 

UserDbManager udm = new UserDbManager();
List<Plik> filelist = udm.fileList(request.getUserPrincipal().getName());

%>

	<div class="content">
		<div class="content_top">
			<h2>Twoje Materiały</h2>
		</div>
		<div class="content_diff">
			<h4>Dodane Pliki</h4>
<table>
<tr><td>Nazwa</td><td>Typ pliku</td><td>Akcja</td></tr>
<% for(int i = 0; i<filelist.size(); i++) 
{
	%>
<tr><td><%= filelist.get(i).getNazwa() %></td><td><%= filelist.get(i).getTyp() %></td><td><a href="../Filedownloadservlet?id=<%= filelist.get(i).getIdplik() %>" >Pobierz</a><br><a href="#" >Usuń</a></td></tr>

	<% 
}

%>
</table>

		</div>

		<div class="content_bottom"></div>
	</div>

	<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>