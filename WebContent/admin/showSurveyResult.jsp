<%@page import="pl.testy.model.Ankieta"%>
<%@page import="java.util.List"%>
<%@page import="pl.testy.db.manager.SurveyDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="../files/top.jsp" %>
  </div>
   <%@include file="../files/menu.jsp" %>

     
    <div class="content">
      <div class="content_top">
      <h2>Przegląd Testów</h2>
      </div>
      <div class="content_diff">
        <h4>Lista Wyników</h4>
        <p>Liczby wskazują ilość kliknięć danej odpowiedzi</p>
<% 

SurveyDbManager sdm = new SurveyDbManager();
Ankieta a = sdm.surveyResult(Integer.parseInt(request.getParameter("id")));
%>
<div>
<table>
<tr>
<td>Nazwa Ankiety</td><td><%= a.getNazwa()  %></td></tr>
<tr>
<td>Stan</td><td><%= a.getStan() %></td>
</tr>
</table>
</div>
<% for (int i = 0; i < a.getListapytan().size(); i++  ) { %>
<div>
<p>
<%= a.getListapytan().get(i).getTresc() %>
</p>
<table>
<% for (int j = 0; j < a.getListapytan().get(i).getListaodpowiedz().size(); j++) { %>
<tr><td><%= a.getListapytan().get(i).getListaodpowiedz().get(j).getTresc() %></td><td><%= a.getListapytan().get(i).getListaodpowiedz().get(j).getIlosc_klikniec() %></td></tr>
<% } %>
</table>
</div>
<% } %>
<p>Legenda:</p>
			<menu>
			<li>N Nieaktywny </li>
			<li>A Aktywny</li>
			</menu>
 			</div>

  
    </div>

   <%@include file="../files/foot.jsp" %>
</div>
</body>
</html>

