<%@page import="pl.testy.model.UserFull"%>
<%@page import="pl.testy.db.manager.UserDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon"
	href="<%=request.getContextPath()%>/images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/style/etna.css" type="text/css">
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/jquery1.7.2.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/jquery.ukrywaniepol.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/loadjsRegisterForm.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/validateRegisterForm.js"></script>

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>

	<%
		UserDbManager manager = new UserDbManager();
		String userlogin = request.getUserPrincipal().getName();
		int iduser = manager.getIdByLogin(userlogin);
		UserFull uf = manager.getProfileById(iduser);
	%>
	<!--  rzutowanie obiektu (UserFull) -->


	<div class="content">
		<div class="content_top">
			<h2>Twój profil</h2>
		</div>
		<div class="content_diff">
			<h4>Wypełnij poniższy formularz</h4>

			<form id="formularzrejestracji"
				onsubmit="return onclickregisterbutton()"
				action="../editprofileservlet" method="post">


				<table>
					<tr>
						<td><label>Imię</label></td>
						<td><input id="imie" type=text name="imie"
							value="<%=uf.getImie()%>">
							<div id="err_imie" class="error"></div></td>
					</tr>
					<tr>
						<td><label>Nazwisko</label></td>
						<td><input id="nazwisko" type=text name="nazwisko"
							value="<%=uf.getNazwisko()%>">
							<div id="err_nazwisko" class="error"></div></td>
					</tr>
					<tr>
						<td><label>Login</label></td>
						<td><%=uf.getLogin()%></td>
					</tr>

					<tr>
						<td><label>Adres email</label></td>
						<td><input id="email" type=text name="email"
							value="<%=uf.getEmail()%>">
							<div id="err_email" class="error"></div></td>
					</tr>
					<tr>
						<td><label>Typ konta</label></td>
						<td>Tester</td>
					</tr>

				</table>
				<input type="hidden" name="iduser" value="<%=uf.getId()%>">


				<div>


					<p>
						<input class="przycisk" id="register" type="submit"
							name="register" value="Wyślij">
					</p>
				</div>

			</form>
			<hr>
			<div>
			<h4> Więcej opcji</h4>
			<p> <a href="../deleteaccountservlet?id=<%=uf.getId()%>">Usuń konto</a> <br> <a href="../user/changePassword.jsp"">Zmień hasło</a></p>
			
			</div>

		</div>

		<div class="content_bottom"></div>
	</div>

	<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>

