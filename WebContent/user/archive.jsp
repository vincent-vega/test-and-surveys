<%@page import="pl.testy.db.manager.UserDbManager"%>
<%@page import="pl.testy.model.Wynik"%>
<%@page import="pl.testy.model.TestHeader"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@page import="pl.testy.model.UserHeader"%>
<%@page import="java.util.List"%>
<%@page import="pl.testy.db.manager.TestDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="../files/top.jsp" %>
  </div>
   <%@include file="../files/menu.jsp" %>

     
    <div class="content">
      <div class="content_top">
      <h2>Archiwum</h2>
      </div>
      <div class="content_diff">
        <h4>Lista Wyników</h4>
       
<%
TestDbManager manager = new TestDbManager(); 
UserDbManager udm = new UserDbManager();
int iduser = udm.getIdByLogin(request.getUserPrincipal().getName());
List<Wynik> wyniklist = manager.wynikListUserArchieve(iduser);
%>

<table>
<tr><td>Nazwa testu</td><td>Zdobyty Wynik</td><td>Maksymalna ilość punktów</td><td>Start testu</td><td>Zakończenie testu</td><td>Akcja</td>
<% for (Wynik w: wyniklist ) { %>
<tr><td><%= w.getNazwatestu() %></td><td><%= w.getZdobytywynik() %></td><td><%= w.getMaxpunkty()  %></td><td><%= w.getDatastart() %></td><td><%= w.getDatastop() %></td><td><a href="addToArchieve.jsp?id=<%= w.getIdwyniku() %>">Usuń</a></td></tr>
<% } %>
</table>


  
    </div>
</div>
   <%@include file="../files/foot.jsp" %>
</div>
</body>
</html>

