<%@page import="pl.testy.model.TestHeader"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online ! </title>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" >

</head>
<body>
<div class="external">
  <%@include file="../files/top.jsp" %>
  </div>
   <%@include file="../files/menu.jsp" %>

<% TestHeader th = (TestHeader)request.getAttribute("testheader") ;%>
     
    <div class="content">
      <div class="content_top">
      <h2><img src="images/globe_32x32.png" />Przegląd Ankiet</h2>
      </div>
      <div class="content_diff">
        <h4>Podejmi test</h4>
   <table>
<tr><td>Nazwa</td><td>Opis</td><td>Czas trwania</td><td>Akcja</td>
<tr><td><%= th.getNazwa() %></td><td><%= th.getOpis() %></td><td><%= th.getCzastrwania() %></td><td><a href="../user/assasmenttestservlet?id=<%= th.getIdtestu() %>">Start!</a></td></tr>

</table>
 			</div>

      <div class="content_bottom"></div>
    </div>

   <%@include file="../files/foot.jsp" %>
</div>
</body>
</html>

