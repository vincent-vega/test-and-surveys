<%@page import="pl.testy.db.manager.UserDbManager"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon"
	href="<%=request.getContextPath()%>/images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/style/etna.css" type="text/css">
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/jquery1.7.2.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/jquery.ukrywaniepol.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/loadjsRegisterForm.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/jscript/validateRegisterForm.js"></script>

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>

	<%
		UserDbManager manager = new UserDbManager();
		String userlogin = request.getUserPrincipal().getName();
		int iduser = manager.getIdByLogin(userlogin);
		
	%>
	<!--  rzutowanie obiektu (UserFull) -->


	<div class="content">
		<div class="content_top">
			<h2>Zmiana hasła</h2>
		</div>
		<div class="content_diff">
			<h4>Wypełnij poniższy formularz</h4>

			<form id="formularzrejestracji"
				onsubmit="return onclickregisterbutton()"
				action="../changepasswordservlet" method="post">


				<table>
					<tr>
						<td><label>Podaj stare hasło</label></td>
						<td><input id="oldpassword" type=text name="oldpassword"
							value="">
							<div id="err_oldpassword" class="error"></div></td>
					</tr>
					<tr>
						<td><label>Podaj nowe hasło</label></td>
						<td><input id="newpassword" type="password" name="newpassword"
							value="">
							<div id="err_newpassword" class="error"></div></td>
					</tr>


					<tr>
						<td><label>Powtórz nowe hasło</label></td>
						<td><input id="retypenewpassword" type="password" name="retypenewpassword"
							value="">
							<div id="err_retypenewpassword" class="error"></div></td>
					</tr>


				</table>
				<input type="hidden" name="iduser" value="<%= iduser %>">


				<div>


					<p>
						<input class="przycisk" id="register" type="submit"
							name="register" value="Wyślij">
					</p>
				</div>

			</form>
		


		</div>

		<div class="content_bottom"></div>
	</div>

	<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>

