<%@page import="pl.testy.model.Odpowiedz"%>
<%@page import="java.util.List"%>
<%@page import="pl.testy.model.Pytanie"%>
<%@page import="pl.testy.model.TestHeader"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stwórz swój test online !</title>
<link rel="shortcut icon" href="../images/favicon.ico"
	type="image/x-icon">
<link rel="stylesheet" href="../style/etna.css" type="text/css" />
<script type="text/javascript" src="../jscript/stoper.js"></script>

</head>
<body>
	<div class="external">
		<%@include file="../files/top.jsp"%>
	</div>
	<%@include file="../files/menu.jsp"%>

	<%
		Pytanie p = (Pytanie) request.getAttribute("pytanie");
		List<Integer> listaid = (List<Integer>) request
				.getAttribute("listapytan");
		List<Integer> odp = (List<Integer>) request
				.getAttribute("udzieloneodpowiedzi");
		Integer indexp = (Integer) request.getAttribute("indexpytanie");
	%>

	<div class="content">
		<div class="content_top">
			<h2>
				Wypełnianie testu
			</h2>
		</div>
		<div class="content_diff">
		<div id="time"><%=request.getAttribute("czas")%></div>
		
			<h4>Test</h4>
			<form action="assasmenttestservlet" method="post">
			<input type="hidden" value="<%=request.getAttribute("czas")%>"
					name="czas" id="czas_input"/> 
				<input type="hidden" value="<%=indexp%>"
					name="indexpytanie" /> 
					
					<input type="hidden" value="<%=request.getAttribute("testId")%>"
					name="testId" /> 
					
					 <input type="hidden"
					value="<%=odp.size()%>" name="iloscodpowiedzi" /> <input
					type="hidden" value="<%=listaid.size()%>" name="iloscpytan" />
				<%
					for (int i = 0; i < listaid.size(); i++) {
				%>
				<input type="hidden" value="<%=listaid.get(i)%>"
					name="idpytania_<%=i%>" />

				<%
					}
				%>
				
				<%
					for (int i = 0; i < odp.size(); i++) {
				%>
				<input type="hidden" value="<%=odp.get(i)%>"
					name="idodpowiedzi_<%=i%>" />

				<%
					}
				%>
				<br />
				<h4>Treść pytania</h4>
				<% if (!"".equals(p.getFoto()))
				{
					
				%>
				
				<img src="<%= p.getFoto() %>"/>
				<% } %>

				
				<%= p.getVideo() %>
				
				
				
				<div>
					<%=p.getTresc()%>
				</div>
				<%
					for (Odpowiedz o : p.getListaodpowiedz()) {
				%>

				<%=o.getTresc()%>
				<input name="odpowiedz" value="<%=o.getIdodpowiedzi()%>"
					type="radio"><br />
				<%
					}
				%>

				<%
					if ((indexp + 1) == listaid.size()) {
				%>
				<input type="submit" value="Koniec" />
				<%
					} else {
				%>
				<input type="submit" value="Następne pytanie" />
				<%
					}
				%>
			</form>
		</div>

		<div class="content_bottom"></div>
	</div>

	<%@include file="../files/foot.jsp"%>
	</div>
</body>
</html>

